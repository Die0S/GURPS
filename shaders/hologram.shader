
HEADER
{
	Description = "";
}

FEATURES
{
	#include "common/features.hlsl"
}

MODES
{
	VrForward();
	Depth(); 
	ToolsVis( S_MODE_TOOLS_VIS );
}

COMMON
{
	#ifndef S_ALPHA_TEST
	#define S_ALPHA_TEST 1
	#endif
	#ifndef S_TRANSLUCENT
	#define S_TRANSLUCENT 0
	#endif
	
	#include "common/shared.hlsl"
	#include "procedural.hlsl"

	#define S_UV2 1
	#define CUSTOM_MATERIAL_INPUTS
}

struct VertexInput
{
	#include "common/vertexinput.hlsl"
	float4 vColor : COLOR0 < Semantic( Color ); >;
};

struct PixelInput
{
	#include "common/pixelinput.hlsl"
	float3 vPositionOs : TEXCOORD14;
	float3 vNormalOs : TEXCOORD15;
	float4 vTangentUOs_flTangentVSign : TANGENT	< Semantic( TangentU_SignV ); >;
	float4 vColor : COLOR0;
};

VS
{
	#include "common/vertex.hlsl"

	PixelInput MainVs( VertexInput v )
	{
		PixelInput i = ProcessVertex( v );
		i.vPositionOs = v.vPositionOs.xyz;
		i.vColor = v.vColor;

		VS_DecodeObjectSpaceNormalAndTangent( v, i.vNormalOs, i.vTangentUOs_flTangentVSign );

		return FinalizeVertex( i );
	}
}

PS
{
	#include "common/pixel.hlsl"
	
	SamplerState g_sSampler0 < Filter( ANISO ); AddressU( WRAP ); AddressV( WRAP ); >;
	SamplerState g_sSampler1 < Filter( TRILINEAR ); AddressU( WRAP ); AddressV( WRAP ); >;
	CreateInputTexture2D( BaseColor, Srgb, 8, "None", "_color", "Base Color,1/,0/1", Default4( 1.00, 1.00, 1.00, 1.00 ) );
	CreateInputTexture2D( Mask, Srgb, 8, "None", "_color", "Base Color,1/Mask,1/3", Default4( 0.00, 0.00, 0.00, 1.00 ) );
	CreateInputTexture2D( SelfIllum, Srgb, 8, "None", "_color", "Self Illum,2/,1/1", Default4( 0.00, 0.00, 0.00, 1.00 ) );
	CreateInputTexture2D( Opacity, Srgb, 8, "None", "_trans", "Opacity,3/,0/0", Default4( 1.00, 1.00, 1.00, 1.00 ) );
	CreateInputTexture2D( Normal, Srgb, 8, "None", "_normal", "MISC,4/,0/4", Default4( 0.50, 0.50, 1.00, 1.00 ) );
	CreateInputTexture2D( Roughness, Linear, 8, "None", "_rough", "MISC,4/,0/2", Default4( 1.00, 1.00, 1.00, 1.00 ) );
	CreateInputTexture2D( Metallic, Linear, 8, "None", "_metal", "MISC,4/,0/1", Default4( 0.00, 0.00, 0.00, 1.00 ) );
	Texture2D g_tBaseColor < Channel( RGBA, Box( BaseColor ), Srgb ); OutputFormat( DXT5 ); SrgbRead( True ); >;
	Texture2D g_tMask < Channel( RGBA, Box( Mask ), Srgb ); OutputFormat( DXT5 ); SrgbRead( True ); >;
	Texture2D g_tSelfIllum < Channel( RGBA, Box( SelfIllum ), Srgb ); OutputFormat( DXT5 ); SrgbRead( True ); >;
	Texture2D g_tOpacity < Channel( RGBA, Box( Opacity ), Srgb ); OutputFormat( DXT5 ); SrgbRead( True ); >;
	Texture2D g_tNormal < Channel( RGBA, Box( Normal ), Srgb ); OutputFormat( DXT5 ); SrgbRead( True ); >;
	Texture2D g_tRoughness < Channel( RGBA, Box( Roughness ), Srgb ); OutputFormat( DXT5 ); SrgbRead( True ); >;
	Texture2D g_tMetallic < Channel( RGBA, Box( Metallic ), Srgb ); OutputFormat( DXT5 ); SrgbRead( True ); >;
	bool g_bMASK < UiGroup( "Base Color,1/Mask,1/2" ); Default( 0 ); >;
	float g_flSelfIllumPower < UiType( Slider ); UiGroup( "Self Illum,2/,1/1" ); Default1( 0 ); Range1( 0, 10 ); >;
	float g_flFresnelPower < UiGroup( "Self Illum,2/Fresnel,2/5" ); Default1( 3 ); Range1( 0, 10 ); >;
	float4 g_vFresnelColor < UiType( Color ); UiGroup( "Self Illum,2/Fresnel,2/4" ); Default4( 1.00, 1.00, 1.00, 1.00 ); >;
	bool g_bFresnel_effect < UiGroup( "Self Illum,2/Fresnel,2/3" ); Default( 0 ); >;
	float4 g_vAO < UiType( Color ); UiGroup( ",0/,0/0" ); Default4( 0.00, 0.00, 0.00, 1.00 ); >;
	
	float4 MainPs( PixelInput i ) : SV_Target0
	{
		Material m;
		m.Albedo = float3( 1, 1, 1 );
		m.Normal = TransformNormal( i, float3( 0, 0, 1 ) );
		m.Roughness = 1;
		m.Metalness = 0;
		m.AmbientOcclusion = 1;
		m.TintMask = 1;
		m.Opacity = 1;
		m.Emission = float3( 0, 0, 0 );
		m.Transmission = 0;
		
		float4 l_0 = Tex2DS( g_tBaseColor, g_sSampler0, i.vTextureCoords.xy );
		float4 l_1 = Tex2DS( g_tMask, g_sSampler1, i.vTextureCoords.xy );
		float4 l_2 = saturate( lerp( l_0, min( 1.0f, (l_0) + (l_1) ), 0.5 ) );
		float4 l_3 = g_bMASK ? l_2 : l_0;
		float4 l_4 = Tex2DS( g_tSelfIllum, g_sSampler0, i.vTextureCoords.xy );
		float l_5 = g_flSelfIllumPower;
		float4 l_6 = l_4 * float4( l_5, l_5, l_5, l_5 );
		float l_7 = g_flFresnelPower;
		float3 l_8 = pow( 1.0 - dot( normalize( i.vNormalWs ), normalize( CalculatePositionToCameraDirWs( i.vPositionWithOffsetWs.xyz + g_vHighPrecisionLightingOffsetWs.xyz ) ) ), l_7 );
		float4 l_9 = g_vFresnelColor;
		float4 l_10 = float4( l_8, 0 ) * l_9;
		float4 l_11 = g_bFresnel_effect ? l_10 : float4( 0, 0, 0, 0 );
		float4 l_12 = lerp( l_6, l_11, 0.7 );
		float4 l_13 = Tex2DS( g_tOpacity, g_sSampler0, i.vTextureCoords.xy );
		float4 l_14 = Tex2DS( g_tNormal, g_sSampler0, i.vTextureCoords.xy );
		float4 l_15 = Tex2DS( g_tRoughness, g_sSampler0, i.vTextureCoords.xy );
		float4 l_16 = Tex2DS( g_tMetallic, g_sSampler0, i.vTextureCoords.xy );
		float4 l_17 = g_vAO;
		
		m.Albedo = l_3.xyz;
		m.Emission = l_12.xyz;
		m.Opacity = l_13.x;
		m.Normal = l_14.xyz;
		m.Roughness = l_15.x;
		m.Metalness = l_16.x;
		m.AmbientOcclusion = l_17.x;
		
		m.AmbientOcclusion = saturate( m.AmbientOcclusion );
		m.Roughness = saturate( m.Roughness );
		m.Metalness = saturate( m.Metalness );
		m.Opacity = saturate( m.Opacity );
		
		return ShadingModelStandard::Shade( i, m );
	}
}
