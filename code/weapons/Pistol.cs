using Sandbox;
using static Sandbox.Input;
using System;

namespace GURPS.Core;

public partial class Pistol : GWeapon
{
	public override float PrimaryRate => 2f;
	public override GWeaponType Type => GWeaponType.SPECIAL;
	public override float SecondaryRate => 1f;
	public override string WorldModel => "weapons/rust_pistol/rust_pistol.vmdl";
	public override string ViewModel => "weapons/rust_pistol/v_rust_pistol.vmdl";


	public Pistol()
	{	
		/*�� ������ ������� ������� ���������� ������ �������������, ��� ��� �������� ���������� � ���������� ��� �� �������� �������� C#*/
		Ammo = 25;
		ConsAmmo = 1;
		PrintName = "��������S";
		CustomHands = "models/first_person/first_person_arms.vmdl";
	}
	
	[ClientRpc]
	protected void ShootEffects()
	{
		Game.AssertClient();
		Particles.Create( "particles/pistol_muzzleflash.vpcf", EffectEntity, "muzzle" );

		ViewModelEntity?.SetAnimParameter( "fire", true );
	}

	public override void PrimaryAttack()
	{

		
	

		if ( !Game.IsClient )
			return;
		//TODO ������, ���� ��������
		var tr = Trace.Ray( Player.AimRay.Position, Player.AimRay.Position + Player.AimRay.Forward * 1000)
			.UseHitboxes()
			.Ignore(this)
			.Run();

		if ( tr.Entity != null)
		{
			Log.Info( (tr.Entity as Pawn)?.GetLimbByHitBox( tr.Hitbox ) );
			Log.Info( tr.Entity?.Health );
		}


	}

	public override void SecondaryAttack()
	{
		ShootEffects();
		Player.SetAnimParameter( "b_attack", true );
		//Player.PlaySound( "rust_pistol.shoot" );
		
		var tr = Trace.Ray( Player.AimRay.Position, Player.AimRay.Position + Player.AimRay.Forward * 1000 )
		.Ignore( this )
		.WithTag("player")
		.Run();

		if ( tr.Hit )
		{
			var newpnw = tr.Entity as Pawn;
			Player.ChangePawn( newpnw );
		}

	}

	protected override void Animate()
	{
		Player.SetAnimParameter( "holdtype", (int)CitizenAnimationHelper.HoldTypes.Pistol );
		Player.SetAnimParameter( "holdtype_handedness", (int)CitizenAnimationHelper.Hand.Both );
	}

	public override void CreateViewModel()
	{
		base.CreateViewModel();
		this.ViewModelArms.Delete();
	}


}
