using Sandbox;

namespace GURPS.Core;

public partial class BlasterSample : GWeapon
{
	public override float PrimaryRate => 12f;
	public override string ViewModel => Cloud.Model( "https://asset.party/facepunch/v_mp5" ).ResourcePath;
	public override string WorldModel => Cloud.Model( "https://asset.party/facepunch/w_mp5" ).ResourcePath;
	public override HoldTypes HoldType => HoldTypes.Rifle;

	public override void Init()
	{
		/*�� ������ ������� ������� ���������� ������ �������������, ��� ��� �������� ���������� � ���������� ��� �� �������� �������� C#*/
		Ammo = 120;
		ConsAmmo = 2;
		base.Init();
		PrintName = "�������";
		CustomHands = "models/first_person/first_person_arms.vmdl";
	}

	[ClientRpc]
	protected virtual void ShootEffects()
	{
		Game.AssertClient();
		ViewModelEntity?.SetAnimParameter( "b_attack", true );
		Particles.Create( "particles/pistol_muzzleflash.vpcf", EffectEntity, "muzzle" );
	}

	public override void PrimaryAttack()
	{ 
		//ShootEffects(To.Everyone);
		ShootEffects();
		Player.SetAnimParameter( "b_attack", true );
		Player.PlaySound( "rust_pistol.shoot" );
		ShootBullet( 0.1f, 100, 20, 1 );

	}

}
