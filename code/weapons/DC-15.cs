using Sandbox;
using static Sandbox.CitizenAnimationHelper;

namespace GURPS.Core;

public partial class DC15A : GWeapon
{
	public override float PrimaryRate => 12f;
	public override string ViewModel => "models/reizer_characters/88_team/star_wars/weapons/v_dc15a.vmdl";
	public override string WorldModel => "models/reizer_characters/88_team/star_wars/props/weapons/dc15a_prop.vmdl";
	public override HoldTypes HoldType => HoldTypes.Rifle;
	public override GWeaponType Type => GWeaponType.Heavy;
	public override Vector3 WeaponOffset { get; set; } = new Vector3( 0, 5, 5 );

	public override Vector2 KickBack { get; set; } = new Vector2( 0.2f, 2f );
	public override Vector3 ImpactForce { get; set; } = new Vector3( 10, 0,0 );


	//public override Vector3 WeaponOffset { get; set; } = new Vector3( -5, 8.5f, 5 );

	public override void Init()
	{
		/*�� ������ ������� ������� ���������� ������ �������������, ��� ��� �������� ���������� � ���������� ��� �� �������� �������� C#*/
		Ammo = 120;
		ConsAmmo = 2;
		base.Init();
		PrintName = "�������";
	}

	[ClientRpc]
	protected virtual void ShootEffects()
	{

		Game.AssertClient();
		ViewModelEntity?.SetAnimParameter( "fire", true );
		//ViewModelEntity?.SetAnimParameter( "fire", true );
		Particles.Create( "particles/pistol_muzzleflash.vpcf", EffectEntity, "muzzle" );
	}

	public override void PrimaryAttack()
	{
		ShootEffects();
		Player.SetAnimParameter( "b_attack", true );

		PlaySound( "sounds/weapons/dc15a/dc15a_blast.sound" );
		ShootBullet( 0.1f, 25, 5, 1 );

	}



}
