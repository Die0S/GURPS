using GURPS.Inventory;
using Sandbox;
using System.Collections.Generic;


//[GameResource( "Based Data", "data", "Describes an item of clothing and implicitly which other items it can be worn with." )]
//public class WeaponS : GameResource
//{


//	public string Title { get; set; }

//	public Curve curve { get; set; }


//	[ResourceType( "vmdl" )]
//	public string Model { get; set; }

//	[HideInEditor]
//	public int Amount { get; set; }

//	[JsonIgnore]
//	public int MySecretNumber => 10;

//	// ...


//}


[GameResource( "Curves Data", "data", "������ ������ ������ � �� ���������" )]
public class CurvesData : GameResource
{
	public Curve WalkShake { get; set; }
	public Curve AngleWalk { get; set; }
	public Curve LandingShake { get; set; }
	public Curve DamageShake { get; set; }
	public Curve FOVWalk { get; set; }


}


[GameResource( "Limits", "limits", "������ ������� �� �������� ��� ��������� ��� ������" )]
public class ItemsLimit : GameResource
{
	public Dictionary<string, int> Limit { get; set; }
}
