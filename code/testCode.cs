using GURPS.Core;
using Sandbox;
using System.IO;


public partial class ProFactor : BaseNetworkable, INetworkSerializer
{
	/// <summary>
	/// ������������� ������������ �������� ������������� ��������
	/// </summary>
	[Net] public float Speed { get; set; } = 1f;
	/// <summary>
	/// ������ �����
	/// </summary>
	[Net] public float Damage { get; set; } = 1f;
	/// <summary>
	/// ��������� ��������
	/// </summary>
	[Net] public float Spread { get; set; } = 1f;
	/// <summary>
	/// ��������� ������� ������
	/// </summary>
	[Net] public float KickBack { get; set; } = 1f;
	/// <summary>
	/// �������� ��������������� ������
	/// </summary>
	[Net] public float EquipSpeed { get; set; } = 1f;
	/// <summary>
	/// �������� �����������
	/// </summary>
	[Net] public float ReloadSpeed { get; set; } = 1f;
	/// <summary>
	/// �������� ������������
	/// </summary>
	public float ZoomSpeed { get; set; } = 1f;


	void INetworkSerializer.Write( Sandbox.NetWrite write )
	{

		write.Write<float>( Damage );
	}

	void INetworkSerializer.Read( ref NetRead read )
	{

		//Damage = read.Read<float>();
		//Log.Info( Damage );

	}
	[ClientRpc]
	public static void Send( byte[] data )
	{
		using ( var stream = new MemoryStream( data ) )
		{
			using ( var reader = new BinaryReader( stream ) )
			{
				//(Game.LocalClient.Pawn as Pawn).FactorsWeapon.Damage = reader.ReadSingle();
			}
		}
	}

	public ProFactor()
	{
	}
}

/*
		FactorsWeapon.Damage = 2;
		using ( var stream = new MemoryStream() )
		{
			using ( var writer = new BinaryWriter( stream ) )
			{
				writer.Write( FactorsWeapon.Damage );
				ProFactor.Send( stream.ToArray() );
			}
		}
*/
