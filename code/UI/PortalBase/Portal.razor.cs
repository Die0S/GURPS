

using Sandbox.UI;
using Sandbox;
using static Sandbox.RenderHook;

namespace GURPS.Misc;

public partial class Portal
{
	public static Portal instance;

	public SceneWorld AvatarWorld => AvatarPanel?.World;
	public ScenePanel AvatarPanel { get; set; }
	public SceneModel Avatar { get; private set; }
	public Portal()
	{

		var model = Model.Load( "models/citizen/citizen.vmdl" );

		instance?.Delete();
		instance = this;
	}

	public override void Tick()
	{
		if ( AvatarPanel == null )
			return;
		//AvatarPanel.Camera.Position = new Vector3( -10, 0, 30 );

		
		AvatarPanel.Camera.Position = new Vector3( -100, 0, 30 );

		AvatarPanel.Camera.ZFar = 6000f;
		AvatarPanel.Camera.FieldOfView = 50f;
		AvatarPanel.Camera.FindOrCreateHook<CloneRender>();

		//AvatarPanel.Camera.RenderTags.Add( "player" );
		//AvatarPanel.Camera.RenderTags.Add( "particles" );

		//AvatarPanel.Camera.ZFar = 6000f;
		//AvatarPanel.Camera.FieldOfView = 50f;
		//Avatar.Rotation = Rotation.LookAt( AvatarPanel.Camera.Rotation.Forward, AvatarPanel.Camera.Rotation.Up );
		//Avatar.Rotation = Rotation.LookAt( AvatarPanel.Camera.Rotation.Forward );
		//Avatar.Rotation = (new Angles(90,0,0) ).ToRotation();
		//AvatarPanel.Camera.RenderTags.Add( "player" );
		//AvatarPanel.Camera.RenderTags.Add( "particles" );

		//AvatarPanel.Camera.BackgroundColor = new Color(1,0,0,0.5f);

		//AvatarPanel.Camera.RenderTags.Add( "world" );


	}

}

