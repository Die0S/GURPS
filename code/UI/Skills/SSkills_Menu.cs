using GURPS.Core;
using Sandbox;
using Sandbox.UI;
using static Sandbox.Gizmo;

namespace GURPS.Skills;

public partial class SkillMenu : RootPanel
{
	public ScenePanel ThePanel;
	public SceneWorld TheWorld;
	public static SkillMenu Instance;

	public SkillMenu()
	{
		ThePanel = new();
		AddChild( ThePanel );

		TheWorld = new SceneWorld();

		//ThePanel.Style.Set( "background-image: url(\"https://wallpapercosmos.com/w/full/0/7/6/1233881-2560x1600-desktop-hd-graph-paper-wallpaper-image.jpg\");" );

		

		ThePanel.Style.Width = Length.Parse( "100%" );
		ThePanel.Style.Height = Length.Parse( "100%" );
		//var ScnemeMp = new SceneMap( AvatarWorld,"facepunch.square" );


		var sun = new SceneSunLight( TheWorld, Rotation.FromPitch( -0 ), Color.White * 1.50f + Color.Cyan * 0.20f );
		sun.ShadowsEnabled = false;
		sun.SkyColor = Color.White * 0.02f + Color.Cyan * 0.02f;

		Instance?.Delete();
		Instance = this;

	}

	[ConCmd.Client( "skills" )]
	public static void skills()
	{
		if ( SkillMenu.Instance != null )
		{
			SkillMenu.Instance.Delete();
			SkillMenu.Instance = null;
		}
		else
			SkillMenu.Instance = new();
	}
}
