

using GURPS.Core;
using Sandbox;
using Sandbox.UI;
using System.Collections.Generic;
using System.Linq;



public class ScenePlayer : SceneBody
{
	public HashSet<SceneBody> BodyParts = new();
	public ScenePlayer( SceneWorld sceneWorld, Model model, Transform transform ) : base( sceneWorld, model, transform )
	{
		Tags.Add( "body" );
		BodyParts.Add( this );
		foreach ( var limb in (Game.LocalPawn as Pawn).Limbs )
		{

			var newmodel = new SceneBody( sceneWorld, model, transform );
			newmodel.Tags.Add( limb.Tags.hlist.Last() );
			// !!!!!!!!!!!!!
			newmodel.limb = limb;
			// !!!!!!!!!!!!!
			newmodel.ScaleBoneSegment_Invert( model.Bones.GetBone( limb.mainboneName ), 0 );
			ScaleBoneSegment( model.Bones.GetBone( limb.mainboneName ), 0 );

			BodyParts.Add( newmodel );
		}
	}

	public void SetAnim(string anim )
	{ 
		foreach(var mdl in BodyParts )
		{
			mdl.CurrentSequence.Name = anim;
		}
	}
	/*
		public override Vector2 GetCenterScreen()
		{

			var direction = thisPanel.Camera.ToScreen( Position.WithZ(Position.z + this.Bounds.Size.z / 1.5f ) + new Vector3() );
			return direction * thisPanel.ScaleFromScreen;
		}
	*/
	public override Vector2 GetCenterScreen()
	{

		var direction = 50;
		return direction * thisPanel.ScaleFromScreen;
	}
}







public class SceneBody : SceneModel {

	public Limb limb;
	public SceneBody( SceneWorld sceneWorld, Model model, Transform transform ) : base( sceneWorld, model, transform )
	{

		Tags.Add( "avatar" );
	
		UseAnimGraph = false;
		Rotation = new Angles( 0, 180, 0 ).ToRotation();
		Position = new Vector3( 0, 0, -40 );
		Update( Time.Delta );
	}

	public int GetBone( Vector3 position )
	{
		Vector3 BestResult = 999;
		int res = 0;
		foreach ( var ds in Model.Bones.AllBones )
		{
			if ( (ds.LocalTransform.Position + Position - position).Length < BestResult.Length && GetBoneWorldTransform(ds.Index).Scale > 0 )
			{
				BestResult = (ds.LocalTransform.Position + Position - position);
				res = ds.Index;
			}

		}
		return res;
	}

	public int GetBoneInRadius( Vector3 position, float radius = 1f )
	{
		Vector3 BestResult = 999;
		int res = 0;
		foreach ( var ds in Model.Bones.AllBones )
		{
			var trns = GetBoneWorldTransform( ds.Index );
			// ds.LocalTransform.Position + Position
		
			if ( trns.Position.Distance( position ) < BestResult.Length && (radius > trns.Position.Distance( position ) ) && trns.Scale > 0 )
			{
				BestResult = trns.Position - position;
				res = ds.Index;
			}

		}
		return res;
	}

	public string GetBoneName( Vector3 position )
	{
		//foreachfor( int i = 0; i < Avatar.Model.Bones.AllBones[1].LocalTransform; i++ )
		string boneName = string.Empty;
		Vector3 BestResult = 999;
		foreach ( var ds in Model.Bones.AllBones )
		{
			if ( (ds.LocalTransform.Position + Position - position).Length < BestResult.Length && GetBoneWorldTransform( ds.Index ).Scale > 0 )
			{
				BestResult = (ds.LocalTransform.Position + Position - position);
				boneName = ds.Name;

			}

		}
		return boneName;
	}

	public IReadOnlyList<BoneCollection.Bone> GetBoneSegment( BoneCollection.Bone bone, List<BoneCollection.Bone> isRetro = null)
	{
		if( isRetro == null )
		{
			isRetro = new();
			isRetro.Add(bone);
		}

		foreach (var bn in bone.Children)
		{
			isRetro.Add(bn);
			if( bn.HasChildren )
			{
				GetBoneSegment(bn, isRetro );
			}
		}
		return isRetro;
	}

	public void ScaleBoneSegment( BoneCollection.Bone bone, float scale)
	{
		foreach(var bn in GetBoneSegment( bone ) )
		{
			SetBoneWorldTransform( bn.Index, GetBoneWorldTransform( bn.Index ).WithScale(scale) );
		}
	}

	public void ScaleBoneSegment_Invert( BoneCollection.Bone bone, float scale )
	{
		var boneSegmeent = GetBoneSegment( bone );
		foreach ( var bn in Model.Bones.AllBones)
		{
			if( !boneSegmeent.Contains(bn))
				SetBoneWorldTransform( bn.Index, GetBoneWorldTransform( bn.Index ).WithScale( scale ) );
		}
	}

	public bool IsChildren( BoneCollection.Bone child, BoneCollection.Bone parent )
	{
		bool result = false;

		if ( child == null || child.Children == null ) return false;
		foreach ( var bn in parent.Children )
		{
			if ( bn.Name == child.Name )
				return true;
			else if ( child.HasChildren )
			{
				result = IsChildren( child, bn );
				if ( result ) return true;
			}
		}
		return result;
	}


	public Vector3 GetCenter()
	{
		Vector3 boneCeenter = Vector3.Zero;
		var bones = GetBoneSegment( Model.Bones.GetBone( limb.mainboneName ) );
		foreach (var bn in bones )
		{
			boneCeenter += GetBoneWorldTransform( bn.Index ).Position;
		}

		boneCeenter /= bones.Count;

		return boneCeenter;
	}	
	

	public void InitPanel(ScenePanel pnl)
	{
		thisPanel = pnl;
	}

	public ScenePanel thisPanel;
	public virtual Vector2 GetCenterScreen()
	{
		Vector3 boneCeenter = Vector3.Zero;
		var bones = GetBoneSegment( Model.Bones.GetBone( limb.mainboneName ) );
		foreach (var bn in bones )
		{
			boneCeenter += GetBoneWorldTransform( bn.Index ).Position;
		}

		boneCeenter /= bones.Count;
	
		var direction = thisPanel.Camera.ToScreen( boneCeenter ) ;

		return direction * thisPanel.ScaleFromScreen;
	}

	public Vector3 GetDirection()
	{
		Vector3 boneCeenter = Vector3.Zero;
		var bones = GetBoneSegment( Model.Bones.GetBone( limb.mainboneName ) );
		foreach ( var bn in bones )
		{
			boneCeenter += GetBodySide();
		}

		boneCeenter /= bones.Count;

		return boneCeenter;
	}


	/// <summary>
	/// Получение стороны направление относительно конечности
	/// </summary>
	/// <returns></returns>
	public Vector3 GetBodySide()
	{
		if(limb.mainboneName.Last() == 'L' )
		{
			return Rotation * Vector3.Left;
		}
		if ( limb.mainboneName.Last() == 'R' )
		{
			return Rotation * Vector3.Right;
		}
		if ( limb.mainboneName == "neck" )
		{
			return Rotation * Vector3.Up;
		}

		return Vector3.Zero;
	}

}

