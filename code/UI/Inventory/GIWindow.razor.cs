using GURPS.Core;
using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Tests;
using System.Collections.Generic;
using System.Linq;

namespace GURPS.Inventory;

public partial class GIWindow : IDrop
{

	public ulong ContainerID;

	public Pawn Player => Game.LocalPawn as Pawn;
	VirtualScrollPanel Scroll;

	public GIWindow(GIContainer container = null) 
	{
		
		if ( container == null )
		{
			container = (Game.LocalPawn as Pawn).Items;
		}
		ContainerID = container.ContainerId;

	}
	
	protected override void OnAfterTreeRender( bool firstTime )
	{
		base.OnAfterTreeRender( firstTime );

		if ( firstTime )
		{
			
			if ( ContainerID == Player.Items.ContainerId )
			{
				Frame.Style.Left = 75;
				Frame.Style.Top = 75;
			}
			else
			{
				/*
				Frame.Style.Left = Screen.Width / 2 ;
				Frame.Style.Top = Screen.Height / 2;
				*/
				Frame.Style.Left = 75;
				Frame.Style.Top = Screen.Height / 1.5f;
			}


			Frame.Style.Dirty();
			
			GURPS.Core.Inventory.instance.Slots.Add(this, Frame.GetContainer() );

			UpdateItems();
		}
	}

	public void UpdateItems()
	{
		if ( Frame.GetContainer() != null )
		{
			foreach ( var child in Frame.GetContainer().Children )
			{
				child.Delete();
				GURPS.Core.Inventory.instance.Slots.Remove( child );
			}
			foreach ( ItemData item in GIS.Containers[ContainerID].ItemList )
			{
				var ittem = new IItem( item, Frame.GetContainer() );
			}
		}
	}


	public override void OnDeleted()
	{
		if ( GURPS.Core.Inventory.instance.MainI.ContainerID != ContainerID )
		{
			Pawn.Disconnect_Serv( ContainerID );
			return;
		}

		if ( Player.Items == null )
		{
			return;
		}
			
	
		if ( ContainerID != Player.Items.ContainerId )
		{
			Pawn.Disconnect_Serv( ContainerID );
		}
		GURPS.Core.Inventory.instance.Slots.Remove( this );
		base.OnDeleted();
	}

	public string Name { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

	public void OnDrop(IItem item )
	{
		if ( item.Item.ContainerId != ContainerID )
		{
			GIS.TransItem( item.Item.Id, ContainerID );
		}
		else
		{
			if ( item.Item != GIS.Containers[ContainerID].ItemList.Last() )
			{
				GIS.MoveItem( item.Item.Id, GIS.Containers[ContainerID].ItemList.Count - 1 );
			}
			else
			{
				UpdateItems();
			}
		}
	}

	public void OnDrag()
	{
		throw new System.NotImplementedException();
	}
}

