using GURPS.Core;
using Sandbox;
using Sandbox.UI;

namespace GURPS.Inventory;

public class MiniInfo : Panel
{
	IItem Item;
	
	public MiniInfo(IItem item)
	{
		//Style.Set( "\tMiniInfo{\r\n\t\tanimation-duration: 0.5s;\r\n\t\tanimation-name: colored;\r\n\t}\r\n\t\r\n\t@keyframes colored {\r\n\t\tfrom {\r\n\t\t\tbackground-color: rgba(0,0,0,0);\r\n\t\t}\r\n\r\n\t\tto {\r\n\t\t\tbackground-color: rgba(0,0,0,0.5);\r\n\t\t}\r\n\t}" );
		Item=item;

		//Style.Set( "animation: infinite rainbow 10s ease-in;" );
		Style.BackgroundColor = new Color( 0, 0, 0, 0.5f );
		Style.Set( "max-width:200px; color:white; \t\tborder: 1px solid black;\r\n\t\tbox-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4); flex-direction: column;" );

		Style.Left = Mouse.Position.x * ScaleFromScreen - Box.Rect.Width;
		Style.Top = Mouse.Position.y * ScaleFromScreen - Box.Rect.Height;

		Style.Position = PositionMode.Absolute;
		Parent = GURPS.Core.Inventory.instance;
		
		var name = new Label();
		name.SetText( item.Item.Instance.Name );
		name.Style.Set( "color:white; \t\tborder: 1px solid black;\r\n\t\tbox-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4);  margin: 0 0 10 0; font-size: 15%;" );
		AddChild( name );

		var desc = new Label();
		desc.SetText( item.Item.Instance.Description );
		AddChild( desc );

		Style.Dirty();
	}
	protected override void OnAfterTreeRender( bool firstTime )
	{
		base.OnAfterTreeRender( firstTime );
	}
	public override void Tick()
	{
		base.Tick();
		
		Style.Left = Mouse.Position.x * ScaleFromScreen - Box.Rect.Width;
		Style.Top = Mouse.Position.y * ScaleFromScreen - Box.Rect.Height;
	}

}
