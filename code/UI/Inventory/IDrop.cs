using GURPS.Core;

namespace GURPS.Inventory;

public interface IDrop
{
	public void OnDrop( IItem item );
}


