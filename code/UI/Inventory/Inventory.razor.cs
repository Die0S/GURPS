using Sandbox.UI;
using Sandbox;
using System;
using System.Collections.Generic;
using GURPS.Inventory;
using System.Linq;
using static Sandbox.Clothing;

namespace GURPS.Core;

public partial class Inventory : IDrop
{

	public List<ulong> ContainersID;

	public static Inventory instance { get; set; }
	//public SceneWorld AvatarWorld { get; private set; }
	public SceneWorld AvatarWorld;
	public ScenePanel AvatarPanel { get; set; }
	public ScenePlayer MainBody { get; set; }
	public Model PlayerModel;
	public Pawn Player => Game.LocalPawn as Pawn;

	public GIWindow MainI;

	public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }


	public DropBox DropPanel;


	public Dictionary<Panel, Panel> Slots { get; set; } = new();


	string[] StandPoses =
	{
		"IdlePose_01_R",
		"IdlePose_01_L",
		"IdlePose_02",
		"IdlePose_03",
		"IdlePose_04_R",
		"IdlePose_04_L",
		"IdlePose_05",
		"IdlePose_06",
		"IdlePose_07_R",
		"IdlePose_07_L",
	};

	public HashSet<Label> TextSegment = new();

	public Inventory()
	{
		AvatarWorld = new SceneWorld();
		PlayerModel = Model.Load( "models/reizer_characters/88_team/star_wars/clones/clone_trp.vmdl" );	
		MainBody = new ScenePlayer( AvatarWorld, PlayerModel, Transform.Zero );


		//var ScnemeMp = new SceneMap( AvatarWorld,"facepunch.square" );


		var sun = new SceneSunLight( AvatarWorld, Rotation.FromPitch( -0 ), Color.White * 1.50f + Color.Cyan * 0.20f );
		sun.ShadowsEnabled = false;
		sun.SkyColor = Color.White * 0.02f + Color.Cyan * 0.02f;

		instance?.Delete();
		instance = this;

		MainBody.SetAnim( "clone_walk_n" );

		//AvatarWorld.DeletePendingObjects();

		DropPanel = new DropBox();

		AddChild( DropPanel );
	}


	public List<HotSlot> HotSlots = new();
	protected override void OnAfterTreeRender( bool firstTime )
	{
		if( firstTime ) {
			Slots.Add( this, AvatarPanel );
			foreach ( var part in MainBody.BodyParts )
			{
				part.InitPanel( AvatarPanel );
				if ( part.limb != null )
				{
					var miniInfo = new MiniCardInfo( part );
					AvatarPanel.AddChild( miniInfo );
				}
			}

			MainBody.InitPanel( AvatarPanel );

			var Info = new BodyCardInfo( MainBody );
			AvatarPanel.AddChild( Info );

			foreach(var sl in Player.Slots.Slots )
			{
				var uislot = new HotSlot( sl );
				AvatarPanel.AddChild(  uislot );
				HotSlots.Add( uislot );
			}
			
		}
		//SetChildIndex( AvatarPanel, Children.Count() );
		base.OnAfterTreeRender( firstTime );
	}


	public override void Tick()
	{

		if ( AvatarPanel == null )
			return;		
		if( AvatarPanel.Camera.Position.Length == 0)
		AvatarPanel.Camera.Position = new Vector3( -65, 0, 5 );

		var rotat = !Input.Down( "walk") ? new Angles( -90, 0, 0 ).ToRotation() : new Angles( 0, 0, 0 ).ToRotation();
		AvatarPanel.Camera.Position += Input.AnalogMove / 2 * AvatarPanel.Camera.Rotation * rotat;
		//OnClicked();


		foreach ( var pl in MainBody?.BodyParts )
		{
			//pl.Update(Time.Delta);
		}
	}

	/// <summary>
	/// ��� ��� ������������ Trace �� ������������ ������� ����� � ������� �� ���������, �� �������� ������ ��������
	/// </summary>
	/// <param name="position"></param>
	/// <param name="ModelPosition"></param>
	/// <returns></returns>
	public string GetBoneName( Vector3 position, Vector3 ModelPosition )
	{
		//foreachfor( int i = 0; i < Avatar.Model.Bones.AllBones[1].LocalTransform; i++ )
		string boneName = string.Empty;
		Vector3 BestResult = 999;
		foreach ( var ds in PlayerModel.Bones.AllBones )
		{
			if ( (ds.LocalTransform.Position + ModelPosition - position).Length < BestResult.Length )
			{
				BestResult = (ds.LocalTransform.Position + ModelPosition - position);
				boneName = ds.Name;

			}

		}
		return boneName;
	}

	public int GetBone( Vector3 position, Vector3 ModelPosition )
	{
		Vector3 BestResult = 999;
		int res = 0;
		foreach ( var ds in PlayerModel.Bones.AllBones )
		{
			if ( (ds.LocalTransform.Position + ModelPosition - position).Length < BestResult.Length )
			{
				BestResult = (ds.LocalTransform.Position + ModelPosition - position);
				res = ds.Index;
			}

		}
		return res;
	}

	protected static Rotation LookAt( Vector3 targetPosition, Vector3 position )
	{
		var targetDelta = (targetPosition - position);
		var direction = targetDelta.Normal;

		return Rotation.From( new Angles(
			((float)Math.Asin( direction.z )).RadianToDegree() * -1.0f,
			((float)Math.Atan2( direction.y, direction.x )).RadianToDegree(),
			0.0f ) );
	}

	public void OnClicked()
	{
		//var sized = ((AvatarPanel.Box.Rect.Size - AvatarPanel.Box.Rect.Position) / Screen.Size) ;
		var sized = AvatarPanel.Box.Rect.Position / Screen.Size ;
		var direction = Screen.GetDirection( ( Mouse.Position - Screen.Size * sized), AvatarPanel.Camera.FieldOfView, AvatarPanel.Camera.Rotation, AvatarPanel.Box.Rect.Size );

		var tr = AvatarWorld.Trace.Ray( AvatarPanel.Camera.Position, direction.EulerAngles.ToRotation().Forward * 500 + AvatarPanel.Camera.Position )
			.WithTag( "avatar" )
			.Run();


		var model = Model.Load( "models/citizen_props/crate01.vmdl" );


		var rsa = new SceneModel( AvatarWorld, model, Transform.Zero );
		var dsa = rsa.Transform;
		dsa.Scale = 0.25f;
		rsa.Transform = dsa;
		rsa.Position = direction.EulerAngles.ToRotation().Forward * 500 + AvatarPanel.Camera.Position;

		rsa.Update( Time.Delta );

	}

	public void OnRigthClick()
	{
		var gModel = GetPartModel();
		if ( gModel == null ) return;
		foreach ( var mod in MainBody.BodyParts )
		{
			mod.ColorTint = Color.White;
		}
		Log.Info( gModel.limb );



		gModel.ColorTint = new Color(1,0,0,0.55f);
	}

	public void OnHover()
	{
		var gModel = GetPartModel();

		if( gModel == null ) return;


		foreach ( var mod in MainBody.BodyParts )
		{
			mod.ColorTint = Color.White;
		}

		//gModel.ColorTint = new Color( 1, 0, 0, 0.55f );
		gModel.ColorTint = new Color( 1, 1, 1, 0.55f );
	}

	protected override void OnMouseOut( MousePanelEvent e )
	{
		base.OnMouseOut( e );

		if( IsInside( Mouse.Position ) )
		{
			foreach ( var mod in MainBody.BodyParts )
			{
				mod.ColorTint = Color.White;
			}
		}
	}

	public KeyValuePair<Panel,Panel> GetSlot(Panel panel = null)
	{
		KeyValuePair<Panel, Panel> best = new();
		foreach (var slot in Slots )
		{
			if (slot.Value.IsInside( Mouse.Position ) )
			{
				if (panel != slot.Key)
				best = slot;
				//return slot;
			}
		}
		return best;
	}

	public GIWindow GetContainer()
	{
		foreach(var pan in Slots )
		{
			var ds = pan.Key as GIWindow;
			if ( ds != null && ds.ContainerID != (Game.LocalPawn as Pawn).Items.ContainerId )
				return ds;
		}
		return null;
	}

	List<SceneObject> ignoreObjects = new();
	SceneObject ResultObj = null;
	public SceneBody GetPartModel( List<SceneObject> isRetro = null )
	{
		var sized = AvatarPanel.Box.Rect.Position / Screen.Size;
		var direction = Screen.GetDirection( (Mouse.Position - Screen.Size * sized), AvatarPanel.Camera.FieldOfView, AvatarPanel.Camera.Rotation, AvatarPanel.Box.Rect.Size );

		//Log.Info( Input.Down( "attack2" ) );'

		var tr = AvatarWorld.Trace.Ray( AvatarPanel.Camera.Position, direction.EulerAngles.ToRotation().Forward * 1000 + AvatarPanel.Camera.Position )
			.WithTag( "avatar" )
			.WithoutTags("ignore")
			.Run();

		if ( !tr.Hit )
			return null;


		var snModel = tr.SceneObject as SceneBody;

		//Log.Info( snModel.GetBoneInRadius( tr.HitPosition, 10 ) );
		if ( snModel.GetBoneInRadius( tr.HitPosition, 8 ) < 1  )
		{
			ignoreObjects.Add( tr.SceneObject );
			tr.SceneObject.Tags.Add( "ignore" );
			ResultObj = GetPartModel();
		}
		else
		{
			ResultObj = tr.SceneObject;
		}

		foreach(var obj in ignoreObjects )
		{
			obj.Tags.Remove( "ignore" );
		}
		ignoreObjects.Clear();
		var copyres = ResultObj as SceneBody;

		ResultObj = null;
		return copyres;
	}

	public void Update()
	{

		foreach(var sl in Slots.ToList())
		{
			(sl.Key as GIWindow)?.UpdateItems();
			//(sl.Value as HotSlot)?.UpdateItems();
		}
		
		
		foreach(var slot in HotSlots.ToList() )
		{
			slot.Delete();
			HotSlots.Remove( slot );
		}
		foreach(var slot in (Game.LocalPawn as Pawn).Slots.Slots )
		{
			var uislot = new HotSlot( slot );
			AvatarPanel.AddChild( uislot );
			HotSlots.Add( uislot );
		}


		foreach ( var slot in GURPS.Core.Inventory.instance.Slots )
		{
			if ( !slot.Key.IsValid )
			{
				GURPS.Core.Inventory.instance.Slots.Remove( slot.Value );
			}
		}
	}

	public void OnDrop(IItem item)
	{
		//Log.Info( item );
	}

	//OnDestroy

}

