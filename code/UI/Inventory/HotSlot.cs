using GURPS.Inventory;
using Sandbox;
using Sandbox.Diagnostics;
using Sandbox.UI;
using Sandbox.UI.Construct;

namespace GURPS.Core;


public class HotSlot : Panel, IDrop
{
	public ItemSlot Slot;
	public IItem Item { get; set; }
	public Image Icon { get; set; }
	public HotSlot(ItemSlot slot, Vector2 SlotSize = new())
	{
		//Style.Set( "width: 2%; height:2%" );
		Style.BackgroundColor = new Color( 0, 0, 0, 0.5f );
		Style.Set( "\t\tborder: 1px solid black;\r\n\t\tbox-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4); margin: 3 3 3 3;" );

		Style.JustifyContent = Justify.Center;
		Style.AlignItems = Align.Center;

		if ( SlotSize.Length < 1 )
			SlotSize = 1;

		Style.Width = Screen.Width * 0.06f * SlotSize.x;
		Style.Height = Screen.Width * 0.06f * SlotSize.y;


		Slot = slot;

		UpdateItems();


		Inventory.instance.Slots.Add( this,this );
	}

	public void UpdateItems() 
	{
		if ( Slot.Item != null )
		{
			Item?.Delete();
			Item = new IItem( Slot.Item, this );
		}
		else
		{
			Item?.Delete();
			Item = null;
		}
	}

	public void OnDrop(IItem item )
	{
		if ( (Game.LocalPawn as Pawn).HasItem( item.Item.Id ) )
		{
			if(Slot.Item != null )
			{
				// TODO ��������� ��������� � ����
				Inventory.instance.Update();
				return;
				
				//? �������� ��� � ������������ �������������� ����� ��� Id ��������
				//GIS.TransItem( Slot.ItemId, (Game.LocalPawn as Pawn).Items.ContainerId );
			}
			GIS.SlotEquip( item.Item.Id, Slot.Name );
			return;
		}
		Inventory.instance.Update();
	}

}
