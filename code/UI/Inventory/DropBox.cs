using GURPS.Inventory;
using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Construct;

namespace GURPS.Core;

public class DropBox : Panel, IDrop
{
	

	public DropBox(  )
	{
		Style.Position = PositionMode.Absolute;
		Style.AlignSelf = Align.FlexEnd;
		Style.Width = Screen.Width / 2;
		Style.Height = Screen.Height / 4;
		Style.BackgroundColor = Color.White;
		Style.Set( "background: rgb(1,0,36, 0.15);" );
		Style.Set( "pointer-events: all;" );
		Style.Left = Screen.Width/2 - Style.Width.Value.Value/2 * 0.6f;
	}



	public void OnDrop( IItem item )
	{
	}


}


