using GURPS.Inventory;
using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Construct;

namespace GURPS.Core;

public class IItem : Panel, IDrop
{
	static int globalID = 0;
	int id;

	public string Name = "";
	public Vector2 ItemSize = new Vector2( 1, 1 );
	public string IconName = "https://static.wikia.nocookie.net/rustarwars/images/7/7a/DC-15_blaster_rifle_-_SW_Card_Trader.png";
	public Color ItemColor = Color.White;

	public Image Icon { get; set; }
	public ItemData Item;
	public Panel Container => Parent;

	public Color RegColor = new Color(0,0,0,0.25f);
	bool WasDragged;

	Panel ColoredBox;

	public IItem( ItemData item, Panel Container)
	{
		Container.AddChild( this );
		Item = item;
		Name = item.Instance.Name;
		ItemSize = item.Instance.Size;
		
		Style.Set( "padding: 3 3 3 3;" );
		Style.Set( "margin: 4 4 4 4;" );
		ColoredBox = new();
		ColoredBox.Style.AlignSelf = Align.Center;
		ColoredBox.Style.Position = PositionMode.Absolute;
		ColoredBox.Style.BackgroundColor = Item.Instance.Color;
		ColoredBox.Style.Width = Screen.Width * 0.005f;
		ColoredBox.Style.Height = Screen.Width * 0.038f * ItemSize.y;

		ColoredBox.Style.Dirty();
/*
		var l = new Label();
		l.Style.FontColor = Color.White;
		l.Style.ZIndex = 100;
		l.Text = item.ContainerId.ToString();
		AddChild( l );
*/
		Icon = Add.Image( item.Instance.Icon == null ? IconName : item.Instance.Icon, "image");
		AddChild( Icon );
		Icon.Style.Set( "\t\tborder: 1px solid black;\r\n\t\tbox-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4); border-radius: 0 6 6 0;" );
		Icon.Style.BackgroundColor = RegColor;
		Icon.AddChild( ColoredBox );

		SetSize();
		id = ++globalID;
		this.Id = id.ToString();

		Inventory.instance.Slots.Add( this, this );
	}

	public void SetSize()
	{
		Style.Width = Screen.Width * 0.04f * ItemSize.x;
		Style.Height = Screen.Width * 0.04f * ItemSize.y;
		//ColoredBox.Style.Height = Box.Rect.Height * 1.1f;
	}

	void OnDrop()
	{
		//var cont = Container.Parent.Parent as GIWindow;
		
		var dropble = (GURPS.Core.Inventory.instance.GetSlot(this).Key as IDrop);

		if ( dropble == null )
		{
			GIS.DropItem( Item.Id );
			Delete();
			return;
		}

		if ( (GURPS.Core.Inventory.instance.GetSlot( this ).Key.Id == this.Id ))
		{
			Delete();
			GURPS.Core.Inventory.instance.Update();
			return;
		}
		
		if ( dropble != null )
		{
			dropble.OnDrop( this );
			Delete();
		}

	}	

	protected override void OnMouseDown( MousePanelEvent e )
	{
		wasCreate?.Delete();
		wasCreate = null;
		if ( Input.Down( "run" )  )
		{
			if( (Game.LocalPawn as Pawn).Items.ContainerId == Item.ContainerId )
			{
				
				GIS.TransItem( Item.Id, Inventory.instance.GetContainer().ContainerID );
				return;
			}
			GIS.TransItem( Item.Id, (Game.LocalPawn as Pawn).Items.ContainerId );
			GURPS.Core.Inventory.instance.Update();
			return;
		}
		WasDragged = !WasDragged;
		Style.Left = Mouse.Position.x * ScaleFromScreen - Style.Width.Value.Value / 2;
		Style.Top = Mouse.Position.y * ScaleFromScreen - Style.Height.Value.Value / 2;
	}
	protected override void OnMouseUp( MousePanelEvent e )
	{
		WasDragged = !WasDragged;
		OnDrop();
	}
	protected override void OnMouseOver( MousePanelEvent e )
	{
		//Log.Info( Curret.Id );
	}

	bool wasChanged = false;
	MiniInfo wasCreate;

	public override void Tick()
	{
		if ( WasDragged )
		{

			Style.Cursor = "move";
			
			Style.Position = PositionMode.Absolute;
			Parent = Inventory.instance;

			Icon.Style.BackgroundColor = new Color( 0.9f, 0.9f, 0, 0.5f );

			Style.Left = Mouse.Position.x * ScaleFromScreen - Style.Width.Value.Value / 2;
			Style.Top = Mouse.Position.y * ScaleFromScreen - Style.Height.Value.Value / 2;


			Parent.Style.Dirty();
		}
		else
		{
			Icon.Style.BackgroundColor = RegColor;
			Style.Cursor = "";
		}

		//! ������� ��������� ��� ����� ������ �������
		wasChanged = IsInside( Mouse.Position );
		if ( wasChanged && !WasDragged )
		{
/*
			if ( wasCreate == null )
				wasCreate = new( this );
*/
		}
		else
		{
			wasCreate?.Delete();
			wasCreate = null;
		}

		Style.Dirty();
	}

	protected override void OnAfterTreeRender( bool firstTime )
	{
		base.OnAfterTreeRender( firstTime );
		if(firstTime)
		SetSize();

	}

	public void OnDrop( IItem item )
	{
		if ( item == this || item.Item.Id == Item.Id )
		{
			GURPS.Core.Inventory.instance.Update();
			return;
		}
		if ( Item.Slot != null )
		{
			// ������ ��� ��� ��� ���
			GURPS.Core.Inventory.instance.Update();
			return;
		}
		if ( Item.ContainerId > 0 && Item.ContainerId == item.Item.ContainerId)
		{
			GIS.MoveItem( item.Item.Id, GIS.Containers[Item.ContainerId].ItemList.IndexOf( Item ) );
			return;
		}


	}


}


