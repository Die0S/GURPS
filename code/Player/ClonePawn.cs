using GURPS.Core;
using GURPS.Inventory;
using Sandbox;
using System.Linq;

public class ClonePawn : Pawn
{
	/// <summary>
	/// ����� ����������
	/// </summary>
	public string Class = string.Empty;

	public ClonePawn()
	{
		
		//ItemsLimit PlayerCurves = ResourceLibrary.GetAll<ItemsLimit>();
		//Limit.Add( "IBackPack", 1);
	}

	
	
	protected override void LoadLimits()
	{
		var PlayerCurves = ResourceLibrary.GetAll<ItemsLimit>();
		ItemsLimit itemlimit = null;

		foreach ( var item in PlayerCurves )
		{
			if ( item.ResourceName == Class )
				itemlimit = item;
		}
		if ( itemlimit == null )
		{
			//itemlimit = ResourceLibrary.Get<ItemsLimit>( "clone.limits" );
			itemlimit = PlayerCurves.FirstOrDefault();
		}
		if ( itemlimit.Limit != null )
		{
			foreach ( var iteml in itemlimit.Limit )
			{
				Limit.Add( GIS.GetInstance( iteml.Key ), iteml.Value );
			}
		}
	}

	public override void Respawn()
	{
		//Tags.Add( "clone" );

		Game.AssertServer();
		SetModel( "models/reizer_characters/88_team/star_wars/clones/clone_trp.vmdl" );

		Health = 100;

		GiveWeapon( new Pistol() );
		GiveWeapon( new DC15A() );
		GiveWeapon( new DC17() );
	}

}
