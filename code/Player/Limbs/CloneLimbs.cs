using GURPS.Effect;
using Sandbox;
using System.Collections.Generic;
using GURPS.Core;
using Sandbox.Internal;

namespace GURPS.SW;

public class CloneHand : Limb
{
	public CloneHand() : base()
	{
		Tags.Add( "arm" );
		DamageToLimb = 0.25f;
		DamageToBody = 0.5f;
	}
	public override float DefMaxHealth { get; set; } = 25;


	/// <summary>
	///  ������ ��� ������ � ���� ������������ ������, ��� �� ������ ��� �� ������������ ����� ������ � ������ ������� �������, ���������� �������������
	/// </summary>
	public class LHand : CloneHand
	{
		public LHand() : base()
		{
			Tags.Add( "left" );
			Name = "#limb.lhand";
			mainboneName = "arm__L";
			PanelOffset = new Vector2( -Screen.Width * 0.01f, -Screen.Height * 0.12f );
		}
	}
	public class RHand : CloneHand
	{
		public RHand() : base()
		{
			Tags.Add( "right" );
			Name = "#limb.rhand";
			mainboneName = "arm_R";
			PanelOffset = new Vector2( -Screen.Width * 0.01f, -Screen.Height * 0.12f );
		}
	}
}

public class CloneLeg : Limb
{
	public CloneLeg() : base()
	{
		Tags.Add( "leg" );
		DamageToLimb = 0.25f;
		DamageToBody = 0.5f;
	}
	public override float DefMaxHealth { get; set; } = 30;

	public class RLeg : CloneLeg
	{
		public RLeg() : base()
		{
			Tags.Add( "right" );
			Name = "#limb.rleg";
			mainboneName = "thigh_R";
			PanelOffset = new Vector2( -Screen.Width * 0.015f, -Screen.Height * 0.05f );
		}
	}

	public class LLeg : CloneLeg
	{
		public LLeg() : base()
		{
			Tags.Add( "left" );
			Name = "#limb.lleg";
			mainboneName = "thigh_L";
			PanelOffset = new Vector2( Screen.Width * 0.015f, -Screen.Height * 0.05f );
		}
	}

}

public class CloneHead : Limb
{
	public CloneHead() : base()
	{
		Tags.Add( "head" );
		DamageToLimb = 0.5f;
		DamageToBody = 1.5f;

		Name = "#limb.head";
		Tags.Add( "head" );
		mainboneName = "neck";

		PanelOffset = new Vector2( Screen.Width * 0.02f, -Screen.Height * 0.05f );

	}
	public override float DefMaxHealth { get; set; } = 20;
}




[Icon( "accessible_forward" )]
public class CloneLimbs : LimbComponent
{

	public Limb Head => GetLimb( 1 );
	public Limb RHand => GetLimb( 2 );
	public Limb LHand => GetLimb( 3 );
	public Limb RLeg => GetLimb( 4 );
	public Limb LLeg => GetLimb( 5 );

	public CloneLimbs()
	{
	}

	public override void OnSpawn()
	{
		CloneHead head = new();

		AddLimb( new CloneHand.LHand() );
		AddLimb( new CloneHand.RHand() );
		AddLimb( new CloneLeg.LLeg() );
		var rlg = new CloneLeg.RLeg();	

		AddLimb(rlg);
		AddLimb( head );

		/*
		rlg.AddEffect( new EBoneBrake( Entity ) );
		head.AddEffect( new EBleedLarge( Entity ) );
		*/
	}
}
