
namespace GURPS.Inventory;


public class IFRAmmo : GItem
{
	public override string Name { get ; set ; } = "#inventory.republic_ammo";
	public override string Icon { get; set; } = "https://static.wikia.nocookie.net/starwars/images/6/62/DC-17-SWBFII.png";
	public IFRAmmo()
	{
	}

}
