//using GURPS.Core;
using GURPS.Core;
using Sandbox;

namespace GURPS.Inventory;


/// <summary>
/// ������� ����� ������������ ��� ����� ������s
/// </summary>
public class GItemWeapon : GItem
{

	public virtual string WeaponClass { get; set; } = "";

	public GItemWeapon()
	{

	}

	public override void Equip( ItemData data, Pawn user = null )
	{
		var weapon = TypeLibrary.Create<GWeapon>( WeaponClass );
		weapon.Ammo = (int)data.GetData( "Ammo" );
		weapon.Item = data;
		user.GiveWeapon( weapon ); 
	}

	public override void Unequip( ItemData data, Pawn user = null )
	{
		var cal = user.WeaponByClass( WeaponClass );
		data.SetData( "Ammo", cal?.Ammo ?? 0 );
		//user.RemoveWeapon( cal );	
	}

	public override void Drop( ItemData data, Pawn dropper = null )
	{
		if ( dropper == null )
			return;
		var weapon = dropper.WeaponByName( WeaponClass );
		
		if ( weapon == null )
		{
			weapon = TypeLibrary.Create<GWeapon>( WeaponClass );
			weapon.Position = dropper.EyePosition + dropper.ViewAngles.Forward * 2;
			weapon.Rotation = dropper.Rotation;
			weapon.SetupPhysicsFromModel( PhysicsMotionType.Dynamic, false );
			weapon.PhysicsGroup.Velocity = dropper.ViewAngles.Forward * 300;
			

			dropper.Unequip( data );
			dropper.RemoveItem( data, false );
		}
		else
		{
			weapon.Drop();
		}
		data.SetData( "Ammo", weapon.Ammo );
		weapon.Item = data;
		weapon.EnableDrawing = true;

	}

	public override void LoadData( ref ItemData item, object target )
	{
		//item.SetData("Ammo", )
	}
}

