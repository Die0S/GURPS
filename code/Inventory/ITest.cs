
namespace GURPS.Inventory;


public class ITest : GItem
{
	public override Vector2 Size { get; set; } = new Vector2( 1, 1 );
	public override string Icon { get; set; } = "https://static.wikia.nocookie.net/starwars/images/a/a4/DC15Acarbine-AltayaFlyer.png";
	public override string Name { get; set; } = "Test";
	public override string Description { get; set; } = "FUllTest ITEM its not work you now";

}
