
namespace GURPS.Inventory;


public class IBackPack : GItem
{
	public override Vector2 Size { get; set; } = new Vector2( 2, 1 );
	public override string Name { get; set; } = "#inventory.republic_backpack1";

}
