using Sandbox;
using GURPS.Effect;
using System.Linq;

namespace GURPS.Core;

public partial class DC17 : GWeapon
{
	public override float PrimaryRate => 2f;
	public override GWeaponType Type => GWeaponType.Light;
	public override float SecondaryRate => 1f;
	public override string WorldModel => "models/reizer_characters/88_team/star_wars/weapons/w_dc17.vmdl";
	public override string ViewModel => "models/reizer_characters/88_team/star_wars/weapons/v_dc17.vmdl";

	//public override Vector3 WeaponOffset { get; set; } = new Vector3( 6, 6, 7 );
	//public override Vector3 WeaponOffset { get; set; } = new Vector3( 7, 8.9f, 6.8f );

	public override HoldTypes HoldType => HoldTypes.Pistol;

	public DC17()
	{	
		/*�� ������ ������� ������� ���������� ������ �������������, ��� ��� �������� ���������� � ���������� ��� �� �������� �������� C#*/
		Ammo = 25;
		ConsAmmo = 1;
		PrintName = "DC-17";
	}
	
	[ClientRpc]
	protected void ShootEffects()
	{
		Game.AssertClient();
		Particles.Create( "particles/pistol_muzzleflash.vpcf", EffectEntity, "muzzle" );

		ViewModelEntity?.SetAnimParameter( "fire", true );
	}

	public override void PrimaryAttack()
	{
		ShootEffects();
		Player.SetAnimParameter( "b_attack", true );
		//Player.PlaySound( "rust_pistol.shoot" );
		Player.GetRandomLimb().AddEffect( new EBoneBrake( Player ) );
	}

	public override void SecondaryAttack()
	{
		Player.RemoveEffect( EBoneBrake.instance, 1);

	}


}
