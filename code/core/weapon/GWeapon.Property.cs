using Sandbox;
using System.Collections.Generic;

namespace GURPS.Core;


public partial class GWeapon
{
	// �������� ���������

	public virtual void Init() { }

	public string PrintName = null;
	/// <summary>
	/// ���������� ��������� � �������
	/// </summary>
	public virtual float PrimaryRate => 1f;
	public virtual float SecondaryRate => 0f;
	/// <summary>
	/// �������� ���������� (����������) ������
	/// </summary>
	public virtual float DeploySpeed => 1f;
	public virtual HoldTypes HoldType => HoldTypes.None;

	// ������� � �������
	/// <summary>
	/// ��� ��������� ���������� ���������� � ������ �������
	/// </summary>
	public virtual bool IsUnlimited => false;
	//public virtual string AmmoType => null;

	/// <summary>
	/// ���������� �������� � ������
	/// </summary>
	[Net] public int Ammo { get; set; } = 0;
	[Net] public int MaxAmmo { get; set; } = 1;
	/// <summary>
	/// ����������� �������� �� �������
	/// </summary>
	public int ConsAmmo { get; set; } = 1;

	public virtual float Damage => 0;
	//public float Damage { get; set; }
	//public virtual string Faction => null;

	/// <summary>
	/// ������ �� ������� ����������� ���������
	/// </summary>
	public virtual Vector3 ImpactForce { get ; set; } = Vector3.Zero;

	/// <summary>
	/// ������������ ������ ������
	/// </summary>
	public virtual Vector2 KickBack { get; set; } = Vector2.Zero;
	/// <summary>
	/// ������ ����������� ������ ������
	/// </summary>
	public virtual Vector2 KBRandomFactor { get; set; } = Vector2.Up;
	/// <summary>
	/// �������� ���������� �� ���� � �������, � ���������� ������ ���� �������������
	/// </summary>
	public virtual string ViewModel => null;
	public virtual string WorldModel=> null;

	

}
