using Sandbox;
using System.Collections.Generic;

namespace GURPS.Core;

[Category( "Hands" )]
[Title( "Hand" ), Icon( "pan_tool" )]
public partial class WeaponViewModel : BaseViewModel
{

	bool IsHands => Model.ResourcePath == Player.HandModelPath;

	Pawn Player => Game.LocalPawn as Pawn;
	public WeaponViewModel( Pawn player )
	{
		SetModel( player.HandModelPath );
		EnableShadowCasting = false;
		EnableViewmodelRendering = true;
	}

	public WeaponViewModel(string hands )
	{
		SetModel( hands );
		EnableShadowCasting = false;
		EnableViewmodelRendering = true;
	}

	protected override void OnDestroy()
	{
		//WeaponsModel.Remove( this );
	}

	public WeaponViewModel( GWeapon weapon )
	{
		base.SetModel(weapon.ViewModel);
		EnableShadowCasting = false;
		EnableViewmodelRendering = true;
	}

	protected float SwingInfluence => 0.05f;
	protected float ReturnSpeed => 5.0f;
	protected float MaxOffsetLength => 10.0f;
	protected float BobCycleTime => 5;
	protected Vector3 BobDirection => new Vector3( 0.0f, 0.5f, 0.25f );
	protected float InertiaDamping => 20f;

	private Vector3 swingOffset;
	private float lastPitch;
	private float lastYaw;
	private float bobAnim;
	private float bobSpeed;

	private bool activated = false;

	public bool EnableSwingAndBob = true;

	public float YawInertia { get; private set; }
	public float PitchInertia { get; private set; }

	public override void PlaceViewmodel()
	{

		if ( !Player.IsValid() )
			return;

		//Camera.Main.SetViewModelCamera( Screen.CreateVerticalFieldOfView( Game.Preferences.FieldOfView ), 2, 500 );
		//Camera.Main.SetViewModelCamera( 85f, 2f, 500 );
		Camera.Main.SetViewModelCamera( Game.Preferences.FieldOfView, 2f, 500 );


		/*		
				var ds = (Vector3.Up * pwn.PawnBody.Maxs.z * Scale) + (pwn.Rotation * pwn.CameraOffset) + pwn.Position;
				var inPos = ds;
				var inRot = pwn.ViewAngles.ToRotation();
		*/
		/*
				var inPos = pwn.EyePosition;
				var inRot = pwn.ViewAngles.ToRotation();
		*/
		var inPos = Camera.Position;
		var inRot = Camera.Rotation;

		//inRot *= new Angles( 5, 10, -15 ).ToRotation();


		if ( !activated )
		{
			lastPitch = inRot.Pitch();
			lastYaw = inRot.Yaw();

			YawInertia = 0;
			PitchInertia = 0;

			activated = true;
		}

		/*
			var cameraBoneIndex = GetBoneIndex( "camera" );
			if ( cameraBoneIndex != -1 )
			{
				var bone = GetBoneTransform( cameraBoneIndex, worldspace: false );
				Camera.Position += bone.Position;
				Camera.Rotation *= bone.Rotation;
			}
		*/

		// �� ��� ������...
		if ( Player.ActiveWeapon.IsValid() && Player.ActiveWeapon.CustomHands == null )
		{
			Position = inPos + Rotation * Player.ActiveWeapon.WeaponOffset;
		}
		else
		{
			Position = inPos;
		}
		
		Rotation = inRot;

		var newPitch = Rotation.Pitch();
		var newYaw = Rotation.Yaw();

		var pitchDelta = Angles.NormalizeAngle( newPitch - lastPitch );
		var yawDelta = Angles.NormalizeAngle( lastYaw - newYaw );

		PitchInertia += pitchDelta;
		YawInertia += yawDelta;

		if ( EnableSwingAndBob )
		{
			var playerVelocity = Player.Velocity;

			var verticalDelta = playerVelocity.z * Time.Delta;
			var viewDown = Rotation.FromPitch( newPitch ).Up * -1.0f;
			verticalDelta *= 1.0f - System.MathF.Abs( viewDown.Cross( Vector3.Down ).y );
			pitchDelta -= verticalDelta * 1.0f;

			var speed = playerVelocity.WithZ( 0 ).Length;
			speed = speed > 10.0 ? speed : 0.0f;
			bobSpeed = bobSpeed.LerpTo( speed, Time.Delta * InertiaDamping );

			var offset = CalcSwingOffset( pitchDelta, yawDelta );
			//offset += CalcBobbingOffset( bobSpeed );

			Position += Rotation * offset;
		}
		else
		{
			SetAnimParameter( "aim_yaw_inertia", YawInertia );
			SetAnimParameter( "aim_pitch_inertia", PitchInertia );
		}

		lastPitch = newPitch;
		lastYaw = newYaw;

		YawInertia = YawInertia.LerpTo( 0, Time.Delta * InertiaDamping );
		PitchInertia = PitchInertia.LerpTo( 0, Time.Delta * InertiaDamping );
	}

	protected Vector3 CalcSwingOffset( float pitchDelta, float yawDelta )
	{
		var swingVelocity = new Vector3( 0, yawDelta, pitchDelta );

		swingOffset -= swingOffset * ReturnSpeed * Time.Delta;
		swingOffset += (swingVelocity * SwingInfluence);

		if ( swingOffset.Length > MaxOffsetLength )
		{
			swingOffset = swingOffset.Normal * MaxOffsetLength;
		}

		return swingOffset;
	}

	protected Vector3 CalcBobbingOffset( float speed )
	{
		bobAnim += Time.Delta * BobCycleTime;

		var twoPI = System.MathF.PI * 2.0f;



		var offset = BobDirection * (speed * 0.005f) * System.MathF.Cos( bobAnim );
		offset = offset.WithZ( -System.MathF.Abs( offset.z ) );

		return offset;
	}

	protected override void OnAnimGraphCreated()
	{
		base.OnAnimGraphCreated();

		SetAnimParameter( "b_deploy", true );

	
	}

	//[ClientRpc]
	//static public void ClearViewModels()
	//{
	//	foreach(var i in AllViewModels )
	//	{

	//		if ( i.IsValid() ) 
	//		{ 
	//			i?.Delete();
	//		}
	//		Log.Info( AllViewModels.Count );
	//	}
	//}
}
