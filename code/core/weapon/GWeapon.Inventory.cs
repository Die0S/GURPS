using GURPS.Inventory;
using Sandbox;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace GURPS.Core;


public partial class GWeapon
{
	public ItemData Item { get; set; }
	
	public virtual Vector2 Size { get; set; }
	public virtual string Icon { get; set; }

	/// <summary>
	/// ������ ����������� ������� �������� ������
	/// </summary>
	public virtual GWeaponType Type => GWeaponType.Average;

	/// <summary>
	/// �������� ���������� �������� ������� ������
	/// </summary>
	/// <returns></returns>
	public static GItem CreateItem(GWeapon weapon)
	{
		var newItem = new GItemWeapon();
		newItem.Name = weapon.Name;
		newItem.Icon = weapon.Icon;
		return null;
	}


	public virtual void Drop()
	{
		Player.Weapons.Remove( this );
		SetParent( null );
		SetupPhysicsFromModel( PhysicsMotionType.Dynamic, false );

		if(Player.ActiveWeapon == this)
		{
			DestroyViewModel( To.Single( Player ) );
			Player.ActiveWeapon = null;
		}
		
		Position = Position + Player.ViewAngles.Forward * 5;
		PhysicsGroup.Velocity = Player.Velocity + Player.ViewAngles.Forward * 200;

		if(Item != null)
		{
			Player.Unequip( Item );
			Player.RemoveItem( Item, false );
		}
		EnableDrawing = true;
		Owner = null;
	}

	public bool OnUse( Entity user )
	{
		if ( Game.IsServer )
		{
			Pawn pawns = user as Pawn;
			if ( pawns != null )
			{
				//pawns.GiveWeapon( this, true );
				//EnableDrawing = false;
				pawns.AddItem( Item );
				Delete();
			}
			return true;
		}
		return false;
	}

}

public enum GWeaponType
{
	Heavy,
	Average,
	Light,
	SPECIAL
}
