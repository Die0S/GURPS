using GURPS.Inventory;
using Sandbox;
using System.Collections.Generic;

namespace GURPS.Core;

public enum HoldTypes
{
	None,
	Pistol,
	Rifle,
	Shotgun,
	HoldItem,
	Punch,
	Swing,
	RPG
}


[Category( "SWeapons" )]
[Icon( "pan_tool" )]
public partial class GWeapon : AnimatedEntity, IUse
{
	// ����������� ����������
	public Pawn Player => Owner as Pawn;

	static float BaseImpact = 2f;
	protected WeaponViewModel ViewModelEntity { get; set; }
	public WeaponViewModel ViewModelArms { get; set; }
	public virtual Vector3 WeaponOffset { get; set;  } = Vector3.Zero;

	//public HoldTypes HoldType;

	/// <summary>
	/// ��������� ������������ ������ (primary � secondary)
	/// </summary>
	public bool IsLocking { get; set; } = false;
	/// <summary>
	/// ������������ �������� ��������, �� ������� ���� ViewModel, �� �������� ���� ������ (World)
	/// </summary>
	public AnimatedEntity EffectEntity => Camera.FirstPersonViewer == Owner ? ViewModelEntity : this;
	
	public string CustomHands = null;

	/// <summary>
	/// ���������� ��� ���������� ������� ������� �� ���������� ��������
	/// </summary>
	[Net, Predicted] public TimeSince TimeSincePrimary{ get; set; }
	[Net, Predicted] public TimeSince TimeSinceSecondary { get; set; }

	
	
	public override void Spawn()
	{
		Init();
		EnableHideInFirstPerson = true;
		EnableShadowInFirstPerson = true;
		EnableDrawing = false;
		Tags.Add( "weapon" );

		if ( WorldModel != null )
		{
			SetModel( WorldModel );
		}
		if ( ImpactForce.Length == 0 )
			ImpactForce = Vector3.Forward * Damage;
	}
	public override void ClientSpawn() {
		Init();
	}
	/// <summary>
	/// ������������ ������
	/// </summary>
	/// <param name="pawns"></param>
	public void Equip( )
	{
		EnableDrawing = true;
		CreateViewModel( To.Single( Player ) );
	}

	public void PickUp( Pawn player, bool setActive = false )
	{

		if ( setActive )
		{
			player.SetActiveWeapon( this );
		}
		this.SetParent( player, true );
		Owner = player;

		PhysicsClear();
	}
	/* *�������* �������� ������������ ���������� ������
		[ClientRpc]
		public void DisablePhysic()
		{
			PhysicsBody.Enabled = false;
		}
	*/
	/// <summary>
	/// ������ ������ ������ (������)
	/// </summary>
	public void Holstering()
	{
		EnableDrawing = false;
		DestroyViewModel( To.Single( Player ) );
	}
	//public void OnHolstering()
/*
	public virtual void Drop()
	{
		DestroyViewModel( To.Single( Player ) );
		Player.ActiveWeapon = null;
		Player.SWeapons.Remove( this );
		SetParent( null );
		SetupPhysicsFromModel( PhysicsMotionType.Dynamic, false );

		//PhysicsBody.Enabled = true;

		Position = Position + Player.ViewAngles.Forward * 5;
		PhysicsGroup.Velocity = Player.Velocity + Player.ViewAngles.Forward * 200;

		Owner = null;
	}
	//public void OnDrop()
*/
	Angles newViewAngle;
	public override void Simulate( IClient player )
	{
		if ( Player.IsValid() )
		{
			Animate();
			if ( CanPrimary() )
			{

				using ( LagCompensation() )
				{
					Ammo -= ConsAmmo;
					TimeSincePrimary = 0;
					PrimaryAttack();
					var newKick = new Vector2(Game.Random.Float(  -1, 1 ) * KickBack.x, Game.Random.Float( KBRandomFactor.x, KBRandomFactor.y ) * KickBack.y );
					newViewAngle = new Angles( newKick.y, newKick.x , 0) ;
				}
			}
			if ( CanSecondary() )
			{
				using ( LagCompensation() )
				{
					TimeSinceSecondary = 0;
					SecondaryAttack();
				}
			}
			/*
			Player.ViewAngles = Player.ViewAngles.LerpTo( Player.ViewAngles - newViewAngle, Time.Delta * 5 );
			newViewAngle = newViewAngle.LerpTo( Angles.Zero, Time.Delta * 5 ); 
			*/

			if ( CanReload() )
					Reload();

		}
	}

	public virtual bool CanReload()
	{
		if ( !Owner.IsValid() || !Input.Down( "reload" ) ) return false;
		return true;
	}

	public virtual void Reload()
	{
		Ammo += 30;
	}

	public override void FrameSimulate( IClient cl )
	{
		Player.ViewAngles = Player.ViewAngles.LerpTo( Player.ViewAngles - newViewAngle, Time.Delta / 0.1f );
		newViewAngle = newViewAngle.LerpTo( Angles.Zero, Time.Delta / 0.1f );
	}

	//TODO !! cl.GetClientData( "g_autoreload" ) !!
	//TODO !! cl.GetClientData( "g_autoreload" ) !!
	//TODO !! cl.GetClientData( "g_autoreload" ) !!

	// TODO �������� ������������ �������� � ����� ����� �����
	public virtual bool CanPrimary()
	{
		if ( !Owner.IsValid() || !Input.Down( "attack1" ) ) return false;
		if (Ammo < 1) return false;
		if ( PrimaryRate <= 0 ) return true;

		return TimeSincePrimary > (1 / PrimaryRate);
	}	
	
	public virtual bool CanSecondary()
	{
		if ( !Owner.IsValid() || !Input.Down( "attack2" ) ) return false;
		if ( SecondaryRate <= 0 ) return true;
		return TimeSinceSecondary > (1 / SecondaryRate);
	}

	public virtual void PrimaryAttack()
	{
	}

	public virtual void SecondaryAttack()
	{
	}
	/// <summary>
	/// ������� ���������� ��� ��������� holdtype-�� � ������������� ���������
	/// </summary>
	protected virtual void Animate()
	{
		Player.SetAnimParameter( "holdtype", (int)HoldType);
	}

	/// <summary>
	/// ��������� ��� ������������ �������
	/// ��������� ������������� ������� ��� �������� ��������� ��������� 
	/// ������ ��������� ��������� ������ �������
	/// </summary>
	public virtual IEnumerable<TraceResult> TraceBullet( Vector3 start, Vector3 end, float radius = 1.0f )
	{
		bool underWater = Trace.TestPoint( start, "water" );

		var trace = Trace.Ray( start, end )
				.UseHitboxes()
				.WithAnyTags( "solid", "player", "npc", "corpes" )
				.Ignore( this )
				.Size( radius );
		if ( !underWater )
			trace = trace.WithAnyTags( "water" );

		var tr = trace.Run();

		if ( tr.Hit )
			yield return tr;
	}

	/// <summary>
	/// ������� �������
	/// </summary>
	public virtual void ShootBullet( Vector3 pos, Vector3 dir, float spread, float force, float damage, float bulletSize )
	{
		var forward = dir;
		forward += (Vector3.Random + Vector3.Random + Vector3.Random + Vector3.Random) * spread * 0.25f;
		forward = forward.Normal;

		foreach ( var tr in TraceBullet( pos, pos + forward * 5000, bulletSize ) )
		{

			tr.Surface.DoBulletImpact( tr );

			//Sound.FromWorld( tr.Surface.AudioMaterial, tr.HitPosition );
			//tr.Surface.DoBulletImpact( tr ).Set( "color", new Vector3( 0f, 0f, 1) );
	

			if ( !Game.IsServer ) continue;
			if ( !tr.Entity.IsValid() ) continue;

			using ( Prediction.Off() )
			{
				var damageInfo = DamageInfo.FromBullet( tr.EndPosition, forward * 100 * force, damage )
					.UsingTraceResult( tr )
					.WithAttacker( Owner )
					.WithWeapon( this );
				tr.Entity.TakeDamage( damageInfo );
				
				if ( tr.Entity.IsWorld )
					return;

				var pwn = tr.Entity as Pawn;
				if ( pwn != null )
				{
					float resist = pwn.FarImpactResist;
					tr.Entity.Velocity += (ImpactForce * BaseImpact * Owner.Rotation) / resist;
					return;
				}
				if ( tr.Body != null )
				{
					tr.Body.ApplyImpulseAt( tr.HitPosition, ImpactForce * 150 * tr.Direction );
				}
			}
		}
	}

	/// <summary>
	/// ������� ������� � �������������� AimRay ( ����������� �������)
	/// </summary>
	public virtual void ShootBullet( float spread, float force, float damage, float bulletSize )
	{
		Game.SetRandomSeed( Time.Tick );
		var ray = Owner.AimRay;
		ShootBullet( ray.Position, ray.Forward, spread, force, damage, bulletSize );

	}


	[ClientRpc]
	public virtual void CreateViewModel()
	{
		if ( ViewModel == null ) return;
		ViewModelEntity = new WeaponViewModel( ViewModel );
		
		//if ( !ViewModelArms.IsValid() )
		//{

		string wephands = Player.HandModelPath == null ? "models/reizer_characters/88_team/star_wars/weapons/hands/v_clone_hands.vmdl" : Player.HandModelPath;
		if ( CustomHands != null )
			wephands = CustomHands;
		ViewModelArms = new WeaponViewModel( wephands );
		ViewModelArms.SetParent( ViewModelEntity, true );
		//}
	}

	[ClientRpc]
	public void DestroyViewModel()
	{
		if ( ViewModelEntity.IsValid() )
		{
			ViewModelEntity.Delete();
		}

		foreach (var d in WeaponViewModel.AllViewModels )
		{
			d.Delete();
		}
		WeaponViewModel.AllViewModels.Clear();
	}
	/// <summary>
	/// ������� �� ������ ��� � ������
	/// </summary>
	protected override void OnDestroy()
	{
		if ( Game.IsClient )
		{
			if ( ViewModelEntity.IsValid() )
			{
				ViewModelEntity.Delete();
			}
		}
	}
/*
	public bool OnUse( Entity user )
	{
		Pawn pawns = user as Pawn;
		if ( pawns != null )
		{
			PickUp( pawns );
			EnableDrawing = false;
		}
		return false;
	}
*/
	public bool IsUsable( Entity user )
	{
		return !Parent.IsValid();
	}


}


