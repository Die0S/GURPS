using Sandbox;
using System.Collections.Generic;

namespace GURPS.Core;

public class MultriTraceWeapon : GWeapon
{

	public override IEnumerable<TraceResult> TraceBullet( Vector3 start, Vector3 end, float radius = 2.0f )
	{
		int shootCount = 10;
		for ( ; shootCount < 1; --shootCount )
		{

			bool underWater = Trace.TestPoint( start, "water" );

			var trace = Trace.Ray( start, end )
					.UseHitboxes()
					.WithAnyTags( "solid", "player", "npc" )
					.Ignore( this )
					.Size( radius );

			//
			// If we're not underwater then we can hit water
			//
			if ( !underWater )
				trace = trace.WithAnyTags( "water" );

			var tr = trace.Run();

			if ( tr.Hit )
				yield return tr;
		}
	}
}
