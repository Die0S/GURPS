﻿using GURPS.Effect;
using GURPS.Inventory;
using Sandbox;
using Sandbox.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GURPS.Core;


public partial class GURPSMode : GameManager
{

	public static RootPanel LocalHUD;
	public GURPSMode()
	{
		GIS.Initialize();
		GEffect.Initialize();
		
		if ( Game.IsClient )
		{
			LocalHUD = new CloneHUD();
		}
	}


	[ConCmd.Server( "respawn" )]
	public static void RespawnComm(  )
	{
			var newPaawn = new Pawn();

			ConsoleSystem.Caller.Pawn?.Delete();
			ConsoleSystem.Caller.Pawn = newPaawn;
			newPaawn.Respawn();

			var spawnpoints = Entity.All.OfType<SpawnPoint>();
			var randomSpawnPoint = spawnpoints.OrderBy( x => Guid.NewGuid() ).FirstOrDefault();

			if ( randomSpawnPoint != null )
			{
				var tx = randomSpawnPoint.Transform;
				tx.Position = tx.Position + Vector3.Up * 50.0f;
				ConsoleSystem.Caller.Pawn.Position = tx.Position;
			}

	}

	[ConCmd.Server( "respawn_clone" )]
	public static void RespawnCommClone()
	{
		var newPaawn = new ClonePawn();

		ConsoleSystem.Caller.Pawn?.Delete();
		ConsoleSystem.Caller.Pawn = newPaawn;
		newPaawn.Respawn();

		var spawnpoints = Entity.All.OfType<SpawnPoint>();
		var randomSpawnPoint = spawnpoints.OrderBy( x => Guid.NewGuid() ).FirstOrDefault();

		if ( randomSpawnPoint != null )
		{
			var tx = randomSpawnPoint.Transform;
			tx.Position = tx.Position + Vector3.Up * 50.0f;
			ConsoleSystem.Caller.Pawn.Position = tx.Position;
		}

	}



	[ConCmd.Server( "spawn_fake" )]
	public static void SpawnFake()
	{
		var newPaawn = new Pawn();
		newPaawn.Respawn();
		if( Game.IsClient )
		{
			ConsoleSystem.Caller.Pawn.Delete( );
		}

		(ConsoleSystem.Caller.Pawn as Pawn).ChangePawn( newPaawn);

		var spawnpoints = Entity.All.OfType<SpawnPoint>();
		var randomSpawnPoint = spawnpoints.OrderBy( x => Guid.NewGuid() ).FirstOrDefault();

		if ( randomSpawnPoint != null )
		{
			var tx = randomSpawnPoint.Transform;
			tx.Position = tx.Position + Vector3.Up * 50.0f;
			newPaawn.Position = tx.Position;
		}

	}


	/*
		// <summary>
		// Включение режима разработчика
		// @todo: Тут будет зевс
		// </summary>
		// <param name = "client" ></ param >
		public override void DoPlayerDevCam( IClient client )
		{

		}
	*/

	[ConCmd.Server( "allEffects" )]
	public static void SetAllEffects()
	{
		/*
		foreach(var eff in GetEffectsClass() )
		{
			(ConsoleSystem.Caller.Pawn as Pawn).AddEffect( TypeLibrary.Create<GEffect>( eff ) );
		}
		*/
		foreach ( var eff in GEffect.Effects)
		{
			(ConsoleSystem.Caller.Pawn as Pawn).AddEffect( TypeLibrary.Create<GEffect>( eff.Value.GetType().Name ) );
		}
	}

	public override void ClientJoined( IClient client )
	{
		base.ClientJoined( client );


		var pawn = new Pawn();
		client.Pawn = pawn;
		pawn.Respawn();

		var spawnpoints = Entity.All.OfType<SpawnPoint>();

		var randomSpawnPoint = spawnpoints.OrderBy( x => Guid.NewGuid() ).FirstOrDefault();
		if ( randomSpawnPoint != null )
		{
			var tx = randomSpawnPoint.Transform;
			tx.Position = tx.Position + Vector3.Up * 50.0f; // raise it up
			pawn.Transform = tx;
		}
	}

	public override void Simulate( IClient cl )
	{
		base.Simulate( cl );
		/*
		var plyPawn = cl.Pawn as Pawn;
		if ( plyPawn.LifeState == LifeState.Dead )
		{
			var modelpl = plyPawn.Corpse;
			if(modelpl != null )
			{
				modelpl.ApplyForce( Input.AnalogMove * 400 );
			}
		}
		*/
	}

	public override bool OnDragDropped( string text, Ray ray, string action )
	{
		base.OnDragDropped( text, ray, action );
		return false;
	}

	public override void FrameSimulate( IClient cl )
	{
		base.FrameSimulate( cl );

	
		//? Переписать на ивенты привязанные к телу?
		
			//Camera.Rotation = LookAt( Game.LocalPawn.Position, Camera.Position) ;
	}

	/// <summary>
	/// Ограничение доступка к Dev
	/// </summary>
	/*	
	public override void DoPlayerDevCam( IClient client )
	{

	}*/


	protected static Rotation LookAt( Vector3 targetPosition, Vector3 position )
	{
		var targetDelta = (targetPosition - position);
		var direction = targetDelta.Normal;

		return Rotation.From( new Angles(
			((float)Math.Asin( direction.z )).RadianToDegree() * -1.0f,
			((float)Math.Atan2( direction.y, direction.x )).RadianToDegree(),
			0.0f ) );
	}




	/* OLD CODE STYLe - К сожалению она не работает на выделенных серверах и тем более клиентах, но оставлю функцию в качестве напоминания.
	public static IEnumerable<string> GetEffectsClass()
	{
		var fileSystems = FileSystem.Mounted.CreateSubSystem( "GEffect" );
		var effects = fileSystems.FindFile( "", "*", true );
		foreach ( var eff in effects )
		{
			StreamReader reader = new StreamReader( fileSystems.OpenRead( eff ) );
			while ( !reader.EndOfStream )
			{
				var line = reader.ReadLine();
				if ( line.Contains( "class" ) && !line.Contains( "//" ) )
				{
					string newLine = String.Empty;
					for ( int index = 0; index <= line.Length; ++index )
					{
						if ( line[index] == 'E' )
						{
							for ( int sindex = index; sindex <= line.Length; ++sindex )
							{
								if ( line[sindex] == ' ' || sindex == line.Length )
									break;
								else
									newLine += line[sindex];
							}
						}
						if ( newLine.Length > 0 )
							break;
					}
					if ( TypeLibrary.GetType( newLine ) != null )
						yield return newLine;
				}
			}
		}
	}
	*/


}
