using Sandbox;
using Sandbox.Effects;
using static Sandbox.RenderHook;

internal class CloneRender : ScreenEffects
{
	public CloneRender()
	{
		
		this.Brightness = 1.0f;
		this.Contrast = 1f;
	
		this.ChromaticAberration.Offset = new Vector3(0,0,0.5f);
		this.ChromaticAberration.Scale = 0.25f;

		this.FilmGrain.Intensity = 0.5f;
		this.FilmGrain.Response = 0f;

		this.HueRotate = 50f;
		
	this.Pixelation = 0.1f;

	this.Saturation = 0f;

	this.Sharpen = 2;

	this.Vignette.Roundness = 0;
	this.Vignette.Color = new Color(0,0,0,0.9f);
	this.Vignette.Intensity = 0.85f;
	this.Vignette.Smoothness = 1f;
	
	}
}
