using Sandbox;
using System;

namespace GURPS.Core;


public partial class GURPSMode 
{

}


public static class GurpsEvent
{


	public static class Player
	{
		public const string Remove = "Remove";

		public class RemoveAttribute : EventAttribute
		{
			public RemoveAttribute() : base( Remove ) { }
		}
	}
}
