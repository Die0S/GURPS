﻿using GURPS.Core;
using Sandbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace GURPS.Effect; 
//BaseNetworkable
// ! Пока оставим без явного сетевого режжима, будем пытаться вручную транслировать необходимые данные нужным игрокам (медикам)
public partial class GEffect : BaseNetworkable
{
	// Создаем инстансы для удобного доступа к конфигурациям эффектов, определяем их в сооответствующем классе
	//public static EBleedLarge instance = new EBleedLarge( null );
	//? Офоормить как статичную?
	public virtual string Name { get; set; }
	public string Description { get; set; }
	public string Icon { get; set; }
	[Net] public int Level { get; set; } = 1;
	[Net] public int MaxLevel { get; set; } = 1;
	/// <summary>
	/// Может ли существовать более одного эземпляра данного эффекта
	/// </summary>
	public bool Stuckble { get; set; } = false;
	/// <summary>
	/// Будет ли данный эффект сохранен по завершению игры
	/// </summary>
	public bool Saveble { get; set; } = false;

	public Limb Limb { get; set; }
	//bool IsActive { get; set; } = false;

	public bool IsLimb => Limb != null;
	//[Net]
	public Entity Object { get; set; }

	public Pawn Player => Object as Pawn;
	public bool IsPlayer => Object is Pawn;

	public virtual void Active() { }
	/// <summary>
	/// Обычно вызывается в OnDestroy, для регистрации состояния
	/// </summary>
	public virtual void Deactive() { }
	/// <summary>
	/// Может ли видить передаваемый игрок
	/// </summary>
	/// <returns></returns>
	public virtual bool CanSee(Pawn pl) { return true; }

	/// <summary>
	/// Функция не требующая постоянного обновления, а переодического вызоова
	/// </summary>
	public virtual void Call() {}

	//public virtual void OnDestroy() {}
	public void AddLevel(int level = 1) 
	{
		var oldLevel = Level;
		var newLevel = Level + level >= MaxLevel ? MaxLevel : Level + level;
		if (newLevel == Level && Stuckble )
		{
			//TODO Исправть StackOverfflow из AddEffect и GetBodyEffect по причине Name
			//var instance = TypeLibrary.Create<GEffect>( this.GetType() );
			//instance.Object = Object;
			//Player.AddEffect( instance );

		}
		Level = newLevel;
		OnChangedLevel( oldLevel, newLevel );
		// TODO Поскольку в С# нельзя удалять обхекты вручнуюю, предеться делать менеджер или еще какую ерись...
	}

	//? Стоит ли добавить OnChangedLevel(n, 0) при удалении объекта
	public virtual void OnChangedLevel( int oldLevel, int newLevel )
	{
	}


	public virtual void OnCreate()
	{
	}

	private static int counter;
	[Net] public int ID { get; set; }
	public GEffect(Entity pl)
	{
		OnCreate();
		if ( pl == null )
		{
			return;
		}
		ID = ++counter;
		Object = pl;
	}
	
	// Система реплиации к сожалению без пустого конструктора обойтись не может... ИСПОЛЬЗОВАТЬ НЕЛЬЗЯ
	public GEffect() { 
		OnCreate();
		ID = ++counter;
	}


	//public static GEffect Instance => EffectSystem.Effects[""];

	public GEffect GetInstance()
	{
		return Effects[GetType().Name];
	}

	/*
		public bool Equals( GEffect other )
		{
			return this.ID == other.ID;
		}

		public static bool operator ==( GEffect item1, GEffect item2 )
		{
			if ( object.ReferenceEquals( item1, item2 ) ) { return true; }
			if ( (object)item1 == null || (object)item2 == null ) { return false; }
			return item1.ID == item2.ID;
		}

		public static bool operator !=( GEffect item1, GEffect item2 )
		{
			return !(item1 == item2);
		}

		public override bool Equals( object obj )
		{
			return Equals( obj as GEffect );
		}

		public override int GetHashCode()
		{
			return ID.GetHashCode();
		}
	*/

	static public Dictionary<string, GEffect> Effects { get; set; } = new();

	public static IEnumerable<string> GetEffectsClass()
	{
		var types = TypeLibrary.GetTypes<GEffect>();
		foreach ( var type in types )
		{
			if ( type.Name.First() == 'E' )
				yield return type.Name;
		}
	}

	public static void Initialize()
	{
		foreach ( var effname in GetEffectsClass() )
		{
			var eff = TypeLibrary.Create<GEffect>( effname );
			GEffect.Effects.Add( eff.GetType().Name, eff );
		}
	}
}


/// <summary>
/// Заготоовка для создания менее нагруженного эффекта
/// </summary>
public class TEffect
{
	public string EffectName;
	public int Level = 1;
	public int MaxLevel = 1;

	protected string className;

	public GEffect Instance => GEffect.Effects[className];

	

	public Limb Limb { get; set; }
	public bool IsLimb => Limb != null;
	public Entity Object { get; set; }

	public Pawn Player => Object as Pawn;
	public bool IsPlayer => Object is Pawn;

	public void AddLevel( int level = 1 )
	{
		var oldLevel = Level;
		var newLevel = Level + level >= MaxLevel ? MaxLevel : Level + level;
		if ( newLevel == Level  )
		{
			//TODO Исправть StackOverfflow из AddEffect и GetBodyEffect по причине Name
			//var instance = TypeLibrary.Create<GEffect>( this.GetType() );
			//instance.Object = Object;
			//Player.AddEffect( instance );

		}
		Level = newLevel;
		// TODO Поскольку в С# нельзя удалять обхекты вручнуюю, предеться делать менеджер или еще какую ерись...
	}

}
