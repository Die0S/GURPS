
using GURPS.Core;
using GURPS.Effect;
using Sandbox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GURPS.Inventory;


//public partial class InventorySystem : BaseNetworkable
public static partial class GIS
{
	/// <summary>
	/// ���������� (Instance) ���� �������
	/// </summary>
	public static Dictionary<string,GItem> GItems = new();
	/// <summary>
	/// ���������� � ���� ������������ ����������
	/// </summary>
	public static Dictionary<ulong,ItemData> Items { get; set; } = new();
	/// <summary>
	/// ���������� � ���� ������������ �����������
	/// </summary>
	public static Dictionary<ulong,GIContainer> Containers = new();
	//public static List<GItem> IItems = new();

	static ulong lastId { get; set; }

	
	public static ItemData Create(string itmeclass)
	{
		if ( !Game.IsServer )
			return null;
		GItem ins;
		GItems.TryGetValue( itmeclass, out ins );
		
		var itemdata = new ItemData( ins );
		itemdata.Id = GenerateID;
		GIS.Items[itemdata.Id] = itemdata;
		
		return itemdata;
	}

	public static ItemData Create( GItem instance )
	{
		if ( Game.IsServer )
		{
			var itemdata = new ItemData( instance );
			itemdata.Id = GenerateID;
			GIS.Items[itemdata.Id] = itemdata;

			return itemdata;
		}
		return null;
	}

	public static ulong GenerateID
	{
		get
		{
			if ( lastId < 1 )
			{
				//... ����� �� �������� ������� ���� data
				lastId = 1;
				//return 0;
			}
			return lastId++;
		}
	}



	/// <summary>
	/// �������� ���������� ������� ������������� ������ ������������ �� I
	/// </summary>
	public static void Initialize()
	{
		foreach(var item in GenerateInstance() )
		{
			var name = item.ToString();
			string newName = string.Empty;
			for ( var i = name.Length - 1; name[i] != '.'; i-- )
				newName += name[i];
			newName = new string( newName.Reverse().ToArray() );
			
			GItems.Add( newName, item);
		}
	}

	public static GItem GetInstance(string Name)
	{
		return GItems[Name];
	}


	public static IEnumerable<GItem> GenerateInstance()
	{
		var items = TypeLibrary.GetTypes<GItem>();
		foreach ( var item in items )
		{
			if ( item.Name.First() == 'I' )
				yield return TypeLibrary.Create<GItem>( item.Name);
		}
	}

	public static void Clear(bool fullclear = false )
	{
		foreach ( var item in Items )
		{
			if ( item.Value == null )
				Items.Remove( item.Key );
		}

		foreach ( var cont in Containers )
		{
			if ( cont.Value == null )
				Containers.Remove( cont.Key );
		}
	}

	[ClientRpc]
	public static void UpdateSlots( byte[] data )
	{
		using ( var stream = new MemoryStream( data ) )
		{
			using ( var reader = new BinaryReader( stream ) )
			{
				(Game.LocalPawn as Pawn).Slots = ItemSlots.Read( reader );
			}
		}
		( Game.LocalPawn as Pawn).InventoryUpdate( 0 );
	}

	[ClientRpc]
	public static void _UpdateItem( ulong itemId, byte[] data )
	{
		using ( var stream = new MemoryStream( data ) )
		{
			using ( var reader = new BinaryReader( stream ) )
			{
				ItemData.Read( reader );
			}
		}
		(Game.LocalPawn as Pawn).InventoryUpdate(0);
	}

	[ClientRpc]
	public static void _DeleteItem( ulong itemId )
	{
		GIS.Items.Remove(itemId );
		(Game.LocalPawn as Pawn).InventoryUpdate( 0 );
	}

	public static void UpdateItem( ulong itemId )
	{
		_UpdateItem( itemId, GIS.Items[itemId].Write().ToArray() );
	}
	/*
		[ClientRpc]
		public static void UpdateSlot( byte[] data )
		{
			using ( var stream = new MemoryStream( data ) )
			{
				using ( var reader = new BinaryReader( stream ) )
				{
					var index = reader.ReadInt32();
					( Game.LocalPawn as Pawn).Slots.Slots[index] = ItemSlot.Read( reader );
				}
			}
		}
	*/

	[ConCmd.Server]
	public static void TransItem( ulong itemId, ulong contID )
	{
		var item = GIS.Items[itemId];
		if ( item.Slot != null )
		{
			var sslot = (ConsoleSystem.Caller.Pawn as Pawn);
			sslot.Unequip( item );
			return;
		}

		GIS.Containers[item.ContainerId].Transmit( item, GIS.Containers[contID] );
	}

	[ConCmd.Server]
	public static void MoveItem( ulong itemId, int newPose )
	{
		var item = GIS.Items[itemId];
		if (item.ContainerId > 0 )
		{
			/*
			if ( contID < 1 )
				contID = item.ContainerId;
			*/
			GIS.Containers[item.ContainerId].MoveTo(item, newPose );
		}
	}

	[ConCmd.Server]
	public static void DropItem( ulong itemId )
	{
		var item = GIS.Items[itemId];
		var ply = (ConsoleSystem.Caller.Pawn as Pawn);
		item.Instance.Drop( item, ply );
		
		if ( item.ContainerId > 0 )
			GIS.Containers[item.ContainerId].Remove( item );
		else
		{	
			item.Destroy();
			ply.Slots.Update();
		}

		_DeleteItem( itemId );
	}

	[ConCmd.Server]
	public static void UseItem( ulong itemId )
	{
		/*	�������� �� �������
			if ((ConsoleSystem.Caller.Pawn as Pawn).HasItem( itemId ) )
			{

			}
		*/
		GIS.Items[itemId].Instance.Use( GIS.Items[itemId], (ConsoleSystem.Caller.Pawn as Pawn) );
	}

	// inItemId ��������� � ������, ���� ���� ���������� � ��������, � �� ���������
	[ConCmd.Server]
	public static void SlotEquip( ulong itemId, string slotName, ulong inItemId = 0 )
	{
		var item = GIS.Items[itemId];
		var ply = (ConsoleSystem.Caller.Pawn as Pawn);
		//TODO ��� �������� ������������ ����������� � ������� ����� ��� ��� ���� �������
		if ( inItemId > 0 )
		{
			GIS.Items[inItemId].Equip( GIS.Items[inItemId].StorageItem.GetSlot( slotName ) );
		}
		else
		{
			ply.Equip( ply.Slots.GetSlot( slotName ), item );
		}
		
	}

	[ClientRpc]
	public static void OpenMenu( ulong contId )
	{
		(Game.LocalPawn as Pawn).CreateInventory( GIS.Containers[contId] );
	}


	public enum IAction
	{
		// ����������� � ������ ������ ����������
		Move,
		// ������ �������� * �� ���� ��� �����
		Take,
		// ������������ ��������
		Drop,
		// ���������� ��������
		Split,
		// ����������� ���������
		Combine,
		// ���������� �������� �� ������ ���������� � ������
		Transfer,
		Destroy,
		// ���������� ��������
		Update
	}
}


