using GURPS.Core;
using Sandbox;
using System.IO;

namespace GURPS.Inventory;

[Title( "Container" ), Icon( "inventory" ), Category( "Containers" )]
public partial class EntityContainer : Prop, IUse
{
	public GIContainer Items;

	public virtual bool MultiConneections { get; set; } = false;

	public bool OpenOnce;

	public override void Spawn()
	{
		base.Spawn();
		Items = new();
		Items.Entity = this;
	}
	public bool IsUsable( Entity user )
	{
		return true;
	}

	public bool OnUse( Entity user )
	{
		if ( Game.IsServer )
		{
			var pawn = (user as Pawn);
			if ( !Items.IsConnected( pawn ) )
			{
				Items.Connect( pawn );

				GIS.OpenMenu(To.Single(user), Items.ContainerId );
				return true;
			}
		}
		return false;
	}

	

	protected override void OnDestroy()
	{
		Items?.Destroy();
		base.OnDestroy();
	}

	public void AddItem( ItemData item )
	{
		if ( item != null )
			Items.Add( item );
	}

	public void RemoveItem( ItemData item )
	{
		if ( item != null )
			Items.Remove( item );
	}

}
