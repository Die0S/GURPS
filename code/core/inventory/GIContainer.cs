using GURPS.Core;
using Sandbox;
using Sandbox.Diagnostics;
using Sandbox.UI;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace GURPS.Inventory;

/// <summary>
/// ��������� ��� ��������� ����������� ��������� ���������
/// </summary>
public partial class GIContainer
{

	public GIContainer(bool isServer = true)
	{
		if ( isServer  )
		{
			ContainerId = GIS.GenerateID;
			GIS.Containers[ContainerId] = this;
		}
	}

	//public GContainer Container { get; }

	public void Connect(Pawn cl )
	{
		if( cl.Client == Entity.Client )
		
		cl.Connect( this );
		Connections.Add( cl.Client );
		Update( To.Single( cl.Client ), Write().ToArray() );
	}

	public void Disconnect( Pawn cl )
	{
		cl.Disconnect( this );
		Connections.Remove( cl.Client );
		Update( To.Single( cl.Client ), ContainerId );
	}

	public bool IsConnected(Pawn cl )
	{
		return Connections.Contains( cl.Client );
	}

	public void Add(ItemData item)
	{
		if ( !Game.IsServer )
			return;
		if( Size > 0 && ItemList.Count + 1 > Size  )
		{
			return;
		}
		ItemList.Add( item );
		item.ContainerId = ContainerId;
		//item.IsValid = true;
		
		Update( To.Multiple( Connections ), Write().ToArray() );
		//GIS.Sync( To.Multiple(Connections), item.Write().ToArray() );
		//GIS.Sync(GIS.IAction.Update, );
	}

	public void Remove( ItemData item, bool DestroyItem = true )
	{
		if ( !Game.IsServer )
			return;

		ItemList.Remove( item );
		if ( DestroyItem )
			item.Destroy();
		else
			item.ContainerId = 0;

		Update( To.Multiple( Connections ), Write().ToArray() );
		//GIS.Sync( To.Multiple( Connections ), GIS.IAction.Destroy, item.Write().ToArray() );
	}

	public void Transmit( ItemData item, GIContainer container, int newPose = -1 )
	{
		Remove( item, false );
		container.Add( item );

		if (newPose > -1)
		{

		}
	}

	public void MoveTo( ItemData item, int newPose )
	{
/*
		var oldItem = GIS.Containers[contID].ItemList.ElementAtOrDefault( newPose );
		// �� ��������, ��� ��� ������� ����������� � ��������
		GIS.Containers[contID].ItemList.Remove(oldItem );
*/
		var TempItem = ItemList.ElementAtOrDefault( newPose );

		ItemList.Remove( item );

		if ( TempItem != null && item != TempItem )
		{
			ItemList.Remove( TempItem );
			ItemList.Add( TempItem );
		}

		ItemList.Insert( newPose, item );

		Update( To.Multiple( Connections ), Write().ToArray() );
	}


	public void Destroy()
	{
		foreach(var items in ItemList.ToList() )
		{
			items.Destroy();
		}
		GIS.Containers.Remove( ContainerId );

		Update( To.Multiple( Connections ), ContainerId );
	}

	public List<IClient> Connections { get; } = new();

	public List<ItemData> ItemList { get;  } = new();
	public HashSet<string> Blacklist { get; set; } = new();
	public HashSet<string> Whitelist { get; set; } = new();
	public ulong ContainerId { get; set; }

	public Entity Entity { get; set; }

	public int Size = 0;


	public static void Read( BinaryReader reader )
	{
		var data = new GIContainer(false);

		data.ContainerId = reader.ReadUInt64();
		data.Entity = reader.ReadEntity();
		data.Size = reader.ReadInt32();
		
		int dcount = reader.ReadInt32();

		for (int i = 0; i < dcount; ++i )
		{
			
			var bytescount = reader.ReadBytes( reader.ReadInt32() );
			using ( var stream = new MemoryStream( bytescount) )
			{
				using ( var writer = new BinaryReader( stream ) )
				{
					data.ItemList.Add( ItemData.Read( writer ));
				}
			}
			
		}

		/*
		foreach ( var itm in GIS.Items )
		{
			if (itm.Value.ContainerId == data.ContainerId)
				GIS.Containers[itm.Value.ContainerId].ItemList.Add( itm.Value );
		}
				HashSet<string> Blacklist { get; set; } = new();
				HashSet<string> Whitelist { get; set; } = new();
		*/

		var pwn = (data.Entity as Pawn);
		if(pwn != null)
		pwn.Items = data;
		
		var cont = (data.Entity as EntityContainer);
		if ( cont != null )
		cont.Items = data;

		GIS.Containers[data.ContainerId] = data;

		(Game.LocalPawn as Pawn).InventoryUpdate( data.ContainerId );

	}

	public MemoryStream Write()
	{
		using ( var stream = new MemoryStream() )
		{
			using ( var writer = new BinaryWriter( stream ) )
			{
				writer.Write( ContainerId );
				writer.Write( Entity );
				writer.Write( Size );

				writer.Write( ItemList.Count);
				foreach ( var itm in ItemList )
				{
					var d = itm.Write().ToArray();
					writer.Write( d.Length );
					writer.Write( d );
				}
				/*
				HashSet<string> Blacklist { get; set; } = new();
				HashSet<string> Whitelist { get; set; } = new();
				*/

				return stream;
			}
		}
	}

	/// <summary>
	/// ���������� ��������� ����������
	/// </summary>
	/// <param name="data"></param>
	[ClientRpc]
	public static void Update( byte[] data )
	{
		using ( var stream = new MemoryStream( data ) )
		{
			using ( var writer = new BinaryReader( stream ) )
			{
				Read( writer );
			}
		}
	}

	/// <summary>
	/// �������� ����������
	/// </summary>
	/// <param name="id"></param>
	[ClientRpc]
	public static void Update( ulong id )
	{
		foreach( var item in GIS.Containers[id].ItemList )
		{
			item.Destroy();
		}

		GIS.Containers.Remove( id );
		GIS.Clear();
	}

}
