﻿using Sandbox;
using GURPS.Core;

namespace GURPS.Inventory;


/// <summary>
/// Структура предмета содержащая минимальную уникальную инфомрацию о предмете
/// </summary>
/// 
[Title( "Item" ), Icon( "inventory" ), Category( "Items" )]
public partial class EntityItem : Prop, IUse
{
	public ItemData Item { get; set; }
	public bool isNotLoot = false;
	public EntityItem(ItemData item)
	{
		Item = item;
		SetModel( Item.Instance.Model != "" ? Item.Instance.Model : "models/citizen_props/trashbag02.vmdl_c" );
		//Item.IsValid = true;
	}


	protected override void OnDestroy()
	{
/*
		if( isNotLoot )
			Item.Destroy();
*/
		base.OnDestroy();
	}

	public EntityItem()
	{
	}

	public bool IsUsable( Entity user )
	{
		return true;
	}

	public bool OnUse( Entity user )
	{
		if ( Game.IsServer )
		{

			if ( user.IsValid && IsValid)
			{
				(user as Pawn).AddItem( Item );
				Delete();
			}
		}
	
		return true;
	}
}
