﻿using GURPS.Core;
using Sandbox;
using System;
using System.Collections.Generic;

namespace GURPS.Inventory;

public partial class GInventory : BaseNetworkable
{
	Pawn Player;


	[Net] public IList<ItemData> Items { get; private set; }

	/// <summary>
	/// Фомирует массив всех экземпляров предметов имеющиехся у игрока
	/// </summary>
	/// <returns></returns>
	public IEnumerable<GItem> GetGItems()
	{
		foreach(var item in Items )
		{
			if(item.Instance != null )
			{
				yield return item.Instance;
			}
			Remove( item );
			throw new ArgumentNullException( "arg", "Откуда у тебя этот предмет?! Но теперь это не важно" );
		}
	}

	public void Remove( ItemData item )
	{
		Items.Remove( item );
		// Save
	}

	//[Net] public IDictionary<string, GItem> EquipItems { get; set; }

	//TODO Пероеписать на net.Write тоолько для Player
	public GInventory()
	{
		
	}

}
