﻿using Sandbox;
using GURPS.Core;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using static Sandbox.Clothing;

namespace GURPS.Inventory;




/// <summary>
/// Реализация предмеета, не хранит перемеенные но хранит все функции сс миром
/// </summary>
public partial class GItem
{

	public virtual string Name { get; set; }
	public virtual string Description { get; set; }
	public virtual string Icon { get; set; }
	public virtual int MaxCount { get; set; } = 1;
	public virtual int ItemCount { get; set; } = 1;

	public virtual bool Savble { get; set; } = false;

	public bool Stackble => MaxCount > 1;

	/// <summary>
	/// Максимальноео количество данного предмета для игрока вне зависимости от размера стака. При 0 - бесконечность
	/// </summary>
	public virtual int MaxItemCount { get; set; } = 0;

	public virtual Vector2 Size { get; set; } = 1;

	public virtual float Weight { get; set; } = 0.5f;

	public virtual GTag Tags { get; set; } = new();

	public virtual Color Color { get; set; } = new( 0, 0, 0, 1 );

	public virtual string Model { get; set; } = string.Empty;

	public virtual void Use(ItemData data, Pawn user = null )
	{
		//Log.Info( Name );
	}

	public virtual void Equip( ItemData data, Pawn user = null )
	{
		Use( data, user );
	}

	public virtual void Unequip( ItemData data, Pawn user = null )
	{
		Deactive(data, user);
	}

	public virtual void Drop( ItemData data, Pawn dropper = null )
	{
		Unequip(data, dropper);
		var item = new EntityItem( data );
		item.Position = dropper.Position + dropper.ViewAngles.Forward * 30;
		item.Rotation = dropper.Rotation;
		item.PhysicsGroup.Velocity = dropper.ViewAngles.Forward * 300;
	}

	public virtual void Deactive( ItemData data, Pawn user = null )
	{
	}

	/// <summary>
	/// Функция для выгрузки кастомных данных, например патроны
	/// </summary>
	public virtual void LoadData(ref ItemData item, object target )
	{
		//item.AddData( "ammo", WeaponAmmo );
	}
}


/// <summary>
/// Динамический тип данных. Содержит динамические данные, которые будетт хранить экземпляр класса, тут будут объявлены все данные, изменяемые по ходу игры
/// </summary>
public class ItemData
{

	public ItemData(GItem instance) 
	{
		Instance = instance;
		Class = GIS.GItems.FirstOrDefault( x => x.Value == instance ).Key;
	}	

	public ItemData()
	{
	}

	/// <summary>
	/// Экзепляр хронящийся в глобальном Хеше системы
	/// </summary>
	public GItem Instance { get; set;}

	public GIContainer Container
	{
		get
		{
			return GIS.Containers[ContainerId];
		}
	}

	/// <summary>
	///  Класс предмета
	/// </summary>
	public string Class {  get; set; }
	public Entity Entity => GIS.Containers[ContainerId]?.Entity;
	public Pawn Player => Entity as Pawn;

	public ulong Id { get; set; }
	public ulong ContainerId { get; set; }
	/// <summary>
	/// Слот в котором находиться предмет
	/// </summary>
	public ItemSlot Slot { get; set; } = null;

	public int Count;
	public float Health;

	/// <summary>
	/// Хранилище для уникальной информации о предмете, не впписаной в рамки класса.
	/// </summary>
	Dictionary<string, float> CustomData = new();
	/// <summary>
	/// Слоты внутри предмета
	/// </summary>
	public ItemSlots StorageItem { get; set; }
	
	public ItemSlot AddSlot(string slotName)
	{
		if( StorageItem == null)
			StorageItem = new ItemSlots(Entity);
		return StorageItem.AddSlot( slotName );
	}

	public void Equip( ItemSlot slot, bool needUpdate = true )
	{
		var container = GIS.Containers[ContainerId];
		ContainerId = 0;

		slot.ItemId = Id;
		Slot = slot;

		Instance.Equip( this, container.Entity as Pawn );

		if ( needUpdate && StorageItem != null )
			StorageItem.Update();

		container.Remove( this, false );
	}


	public void Unequip(ulong contId, bool needUpdate = true )
	{
		ContainerId = contId;

		Slot.ItemId = 0;
		Slot = null;

		Instance.Unequip( this, GIS.Containers[ContainerId].Entity as Pawn );

		if ( needUpdate && StorageItem != null )
				StorageItem.Update();

		GIS.Containers[ContainerId].Add( this );
	}

	public void AddData(string name, float data )
	{
		CustomData.Add( name, data );
	}
	public void SetData( string name, float data )
	{
		CustomData[name] = data;
	}
	public void RemoveData( string name )
	{
		CustomData.Remove(name);
	}

	public float GetData( string name )
	{
		return CustomData.GetValueOrDefault(name);
	}

	public void Destroy()
	{
		//IsValid = false;
		if ( Slot != null )
		{
			Slot.ItemId = 0;
			
		}
		//? Дополнительное удаление можно оптимизировать убрав лишние вызовы
		//GIS._DeleteItem( Id );
		GIS.Items.Remove(Id);
		//GIS.Containers[ContainerId].ItemList.Remove( this );
		//Entity?.Delete();
	}

	public MemoryStream Write()
	{
		using ( var stream = new MemoryStream() )
		{
			using ( var writer = new BinaryWriter( stream ) )
			{
				writer.Write( Id );
				writer.Write( ContainerId );
				
				writer.Write( Class );

				writer.Write( Count );
				writer.Write( Health );
				
				writer.Write( CustomData.Count );
				foreach ( var item in CustomData )
				{
					writer.Write( item.Key );
					writer.Write( item.Value );
				}
				var isval = Slot != null;
				writer.Write( isval );
				if( isval )
					Slot.Write( writer );
				
				isval = StorageItem != null;
				writer.Write( isval );
				if ( isval )
					StorageItem.Write( writer );
				return stream;
			}
		}
	}

	public void Write( BinaryWriter writer )
	{
		writer.Write( Id );
		writer.Write( ContainerId );

		writer.Write( Class );

		writer.Write( Count );
		writer.Write( Health );

		writer.Write( CustomData.Count );
		foreach ( var item in CustomData )
		{
			writer.Write( item.Key );
			writer.Write( item.Value );
		}

		var isval = Slot != null;
		writer.Write( isval );
		if ( isval )
			Slot.Write( writer );
		StorageItem.Write(writer );

		isval = StorageItem != null;
		writer.Write( isval );
		if ( isval )
			StorageItem.Write( writer );
	}
	
	public static ItemData Read( BinaryReader reader)
	{
		var data = new ItemData();

		data.Id = reader.ReadUInt64();
		data.ContainerId = reader.ReadUInt64();
		data.Class = reader.ReadString();
		data.Instance = GIS.GetInstance( data.Class );
		data.Count = reader.ReadInt32();
		data.Health = reader.ReadSingle();

		
		
		int cdcount = reader.ReadInt32();
		Dictionary<string, float> csData = new();
		for (int i = 0; i < cdcount; ++i )
		{
			csData[reader.ReadString()] = reader.ReadSingle();
		}

		data.CustomData = csData;
		if(reader.ReadBoolean())
			data.Slot = ItemSlot.Read( reader );

		if ( reader.ReadBoolean() )
			data.StorageItem = ItemSlots.Read( reader );
		GIS.Items[data.Id] = data;
		
		return data;
	}

}

/// <summary>
/// Система слотов для хрранение предметов внутри других предметов
/// </summary>
public class ItemSlots
{

	public ItemSlots(Entity player = null) { Owner = player as Pawn; }
	Pawn Owner;



	public List<ItemSlot> Slots = new();
	// Более автономный вариант
	//public Pawn Owner => Slots.FirstOrDefault().Item.Entity as Pawn;
	/// <summary>
	/// Есть ли слоты в предмете
	/// </summary>
	public bool IsEmpty => Slots.Count == 0;
	public ItemSlot AddSlot(string name,  string icon = "", bool needUpdate = false)
	{
/*		Уникализация слотов
		foreach ( var slot in Slots )
		{
			if ( slot.Name == name )
				return slot;
		}
*/
		var newslot = new ItemSlot();
		newslot.Name = name;
		//newslot.Icon = icon;
		Slots.Add( newslot );
		if ( needUpdate )
			Update();
		return newslot;
	}

	public ItemSlot GetSlot( string slotName )
	{
		foreach (var slot in Slots )
		{
			if(slot.Name == slotName)
				return slot;
		}
		return null;
	}



	public void Write( BinaryWriter writer)
	{
		var isval = Owner != null;
		writer.Write( isval );
		if ( isval )
			writer.Write( Owner );
		writer.Write( Slots.Count );
		foreach ( var slot in Slots )
		{
			slot.Write( writer );
		}
	}

	public MemoryStream toByte(  )
	{
		var stream = new MemoryStream();
		var writer = new BinaryWriter( stream );

		Write( writer );

		return stream;
	}

	public static ItemSlots Read( BinaryReader reader )
	{
		var isval = reader.ReadBoolean();
		Entity pwn = null;
		if( isval )
			pwn = reader.ReadEntity();
		var data = new ItemSlots( pwn );

		for ( int i = reader.ReadInt32(); i > 0; --i )
		{
			data.Slots.Add( ItemSlot.Read( reader ) );
		}
		return data;
	}

	public void Update()
	{
		//? Owner ??
		GIS.UpdateSlots( To.Multiple( Owner.Items.Connections ), toByte().ToArray() );
		foreach ( var slot in Slots )
		{
			if( slot.ItemId  > 0)
				GIS.UpdateItem( slot.ItemId );
		}

	}

	public void Destroy()
	{
		foreach ( var items in Slots.ToList() )
		{
			if(items.Item != null )
			{
				GIS._DeleteItem( items.Item.Id );
				items.Item.Destroy();
			}
		}
	}

}


public class ItemSlot
{
	public string Name = string.Empty;
	public ulong ItemId = 0;
	public ItemData Item => ItemId > 0 ? GIS.Items[ItemId] : null;
	//public GTag Blacklist;

	public bool Check( ItemData item)
	{
		return item.Instance.Tags.Has( Name );
	}

	public void Write( BinaryWriter writer )
	{
		writer.Write( Name);
		bool isval = ItemId > 0;
		writer.Write( isval );
		if ( isval )
			writer.Write( ItemId );
	}

	public static ItemSlot Read( BinaryReader reader )
	{
		var data = new ItemSlot();
		data.Name = reader.ReadString();
		if ( reader.ReadBoolean() )
			data.ItemId = reader.ReadUInt64();
		return data;
	}
/*
	public byte[] toByte()
	{
		var stream = new MemoryStream();
		var writer = new BinaryWriter( stream );

		writer.Write()
		Write( Count );

		return stream;
	}

	public void Update()
	{
		GIS.UpdateSlot()
	}
*/
}
