﻿using Sandbox.UI;
using Sandbox;
using System.Collections.Generic;
using System;
using System.Numerics;

namespace GURPS.Core;


partial class Pawn
{
	/// <summary>
	/// Для реализации симуляции удерживания мы используем буфер
	/// </summary>
	public Entity UsingEntity { get; set; }
	protected virtual void UseFail()
	{
		if ( Game.IsClient )
		{
			var d = PlaySound( "player_use_fail" );
			d.SetVolume( 0.1f );
		}
	}



	protected virtual void StopUsing()
	{
		UsingEntity = null;
	}
	protected bool IsValidUseEntity( Entity e )
	{
		if ( e == null ) return false;
		if ( e is not IUse use ) return false;
		if ( !use.IsUsable( this ) ) return false;

		return true;
	}

	protected virtual Entity FindUsable()
	{

		var tr = Trace.Ray( EyePosition, EyePosition + EyeRotation.Forward * 85 )
		.Radius( 5 )
		.Ignore( this )
		.Run();

		// Проверка возможности использования родительских энтити, на случай если мы попали в его child
		var ent = tr.Entity;
		while ( ent.IsValid() && !IsValidUseEntity( ent ) )
		{
			ent = ent.Parent;
		}
		// Проверка
		if ( !IsValidUseEntity( ent ) ) return null;

		return ent;
	}

	//WorldInput WorldInput = new();
	protected virtual void PlayerUse()
	{
	
		using ( Prediction.Off() )
		{
			if ( Input.Pressed( "use" ) )
			{
/*
				WorldInput.Ray = AimRay;
				WorldInput.MouseLeftPressed = Input.Down( "use" );
*/
				UsingEntity = FindUsable();

				if ( UsingEntity == null )
				{
					UseFail();
					return;
				}
			}
		
			if ( !Input.Down( "use" ) )
			{
				StopUsing();
				return;
			}

			if ( !UsingEntity.IsValid() )
				return;

			// Если мы отойдем слишком далеко или что-то в этом роде, то, возможно следует ClearUse()?

			//
			// Если use возвращает true, то мы можем продолжать использовать его
			//
			if ( UsingEntity is IUse use && use.OnUse( this ) )
				return;

			StopUsing();
		}
	}
}


