﻿using Sandbox;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using GURPS.Effect;
using GURPS.SW;
using System.Linq;
using System.IO;
using GURPS.Inventory;
using System.Xml.Linq;
using Sandbox.Diagnostics;

namespace GURPS.Core;

public partial class Pawn : AnimatedEntity
{

	public Pawn()
	{
		Tags.Add( "player" );
	}

	[Net]
	public IList<GWeapon> SWeapons { get; set; }
	// Тест без Predicted
	[Net, Predicted]
	public GWeapon ActiveWeapon { get; set; }



	//! Приятным для игры является +- 10 по XY
	public BBox PawnBody
	{
		get => new
		(
			new Vector3( -12, -12, 0 ),
			new Vector3( 12, 12, 64 )
		);
	}

	Vector3 HeadShake;
	//[Browsable( false )]
	/// <summary>
	/// Модель тела персонажа
	/// </summary>
	public BodyModel FModel { get; set; }
	public string HandModelPath = "models/reizer_characters/88_team/star_wars/weapons/hands/v_clone_hands.vmdl";

	public LimbComponent LimbsComponent => Components.Get<LimbComponent>();
	public IList<Limb> Limbs => LimbsComponent?.Limbs;

	[Browsable( false )]
	public Rotation EyeRotation => ViewAngles.ToRotation();

	public override Ray AimRay => new Ray( EyePosition, ViewAngles.Forward );

	//[Net, Predicted, Browsable( false )]
	//[ClientInput]
	public Vector3 EyePosition
	{
		get;
		set;
	}

	[Browsable( false )]
	public Vector3 CameraOffset = new Vector3(0, 0, 0 );

	public CitizenAnimationHelper.HoldTypes HoldType => (CitizenAnimationHelper.HoldTypes)(HoldTypes)GetAnimParameterInt( "holdtype" );

	public override void Spawn()
	{
		base.Spawn();
		CollisionBounds = PawnBody;
		EnableHitboxes = true;
		EnableDrawing = true;
		EnableShadowInFirstPerson = false;
		EnableHideInFirstPerson = true;

		SetupPhysicsFromAABB( PhysicsMotionType.Keyframed, PawnBody.Mins, PawnBody.Maxs );
		//Components.Add( new LimbComponent() );
		Components.Create<CloneLimbs>().Spawn();



		// TODO Настройка  дистанции рендера
		//Camera.Main.ZFar = 20000f;
		//MainAnimation = new CitizenAnimationHelper( this );	
	}

	/// <summary>
	/// Вызывается после ицициализации клиента
	/// </summary>
	public virtual void Respawn()
	{
		Game.AssertServer();
		//SetModel( "models/reizer_characters/88_team/star_wars/clones/clone_trp.vmdl" );
		SetModel( "models/citizen/citizen.vmdl" );
		//Model = Cloud.Model( "https://asset.party/sboxmp/frank_mp" );
		//Health = 100;
/*
		GiveWeapon( new BlasterSample() );
		GiveWeapon( new Pistol() );
		GiveWeapon( new DC15A() );
		GiveWeapon( new DC17() );
*/
		//new AnimatedEntity( "models/w_backpack_01_lod0.vmdl" ).SetParent( this ,"spine_01");

		Health = 100;

		LoadLimits();
		Load();

		ResetInterpolation();
	}

/*
	public override void OnActive()
	{
		base.OnActive();
	}
*/
	public override void ClientSpawn()
	{
		base.ClientSpawn();
		//! Возникает ошибка при первом спавне
		FModel = new BodyModel( this);
		FModel.Draw( true );

		Inventory.instance?.Delete();
		Inventory.instance = null;

		if( !GURPSMode.LocalHUD.IsValid )
		{
			GURPSMode.LocalHUD = new CloneHUD();
		}
	}
	[ClientInput] public Vector3 InputDirection { get; protected set; }
	[ClientInput] public Angles ViewAngles { get; set; }
	[Net] bool bIsLock { get; set; } = false;

	public override void BuildInput()
	{
	

		if ( Input.StopProcessing || bIsLock || IsDead )
		{
			Input.ClearActions();
			InputDirection = 0;
			return;
		}

		InputDirection = Input.AnalogMove;


		var look = Input.AnalogLook;

		var viewAngles = ViewAngles;

		/// Попытаться все же нормализовать look
		// TODO Решить вопрос "фиксированной" камеры
		var lookPitch = viewAngles.pitch + look.pitch < 90f && viewAngles.pitch + look.pitch > -90f ? viewAngles.pitch + look.pitch : ViewAngles.pitch;

		viewAngles = (viewAngles + look).WithPitch( lookPitch );

		//if ( viewAngles.pitch + look.pitch < 90f && viewAngles.pitch + look.pitch > -90f )

		//if ( Camera.FirstPersonViewer != null )
		ViewAngles = viewAngles.Normal;

	}


	public override void Simulate( IClient cl )
	{
		base.Simulate( cl );

		EyePosition = (Vector3.Up * GetBody().Maxs.z) + (Rotation * CameraOffset) + Position;

		
		if ( Input.Pressed( "slot2" ) )
		{
			SetActiveWeapon( GetNextWeapon( GWeaponType.Average ) );
			//RemoveEffect( new EBleedSmall( ), 1 );
			RemoveEffect( EBleedSmall.instance, 1 );
			UnRagdoll();
		}

		if ( IsDead )
			return;

		WalkSimulation();
		AnimatePlayer();
		PlayerUse();
		Rotation = ViewAngles.WithPitch( 0f ).ToRotation();

		if ( Game.IsServer && Input.Pressed( "slot4" ) )
		{
			var ragdoll = new GCorpse();
			ragdoll.SetModel( Model.Name );
			ragdoll.Position = Position + ViewAngles.Forward * 40;
			ragdoll.Rotation = Rotation.LookAt( Vector3.Random.Normal );
			ragdoll.SetupPhysicsFromModel( PhysicsMotionType.Dynamic, false );
			ragdoll.PhysicsGroup.Velocity = ViewAngles.Forward * 1000;
			//Pawn.ScaleBoneSegment_Invert( ragdoll, Model.Bones.GetBone( "arm_upper_L" ), 0 );
		}

		if ( Game.IsServer && Input.Pressed( "slot5" ) )
		{
			var ragdoll = new EntityContainer();
			ragdoll.SetModel( "models/citizen_props/crate01.vmdl_c" );
			ragdoll.Position = Position + ViewAngles.Forward * 60;
			ragdoll.Rotation = Rotation.LookAt( Vector3.Random.Normal );
			ragdoll.PhysicsGroup.Velocity = ViewAngles.Forward * 500;
			ragdoll.AddItem( GIS.Create( "ITest" ) );
			ragdoll.AddItem( GIS.Create( "ItemDC15A" ) );
			ragdoll.AddItem( GIS.Create( "ItemBlasterSample" ) );
			ragdoll.AddItem( GIS.Create( "ItemDC17" ) );
			ragdoll.AddItem( GIS.Create( "ItemPistol" ) );
		}

		if ( Input.Pressed( "drop" ) )
		{
			ActiveWeapon?.Drop();
		}
		if ( Input.Pressed( "slot1" ) )
		{
			SetActiveWeapon( GetNextWeapon( GWeaponType.Heavy ));
			AddEffect( new EBleedSmall( ) );
		}


		//Log.Info( GEffect.AllEffects.Count );
		if ( Input.Pressed( "slot3" )  )
		{
			/*
			if(DirectPlayback.Time >= 0.1f)
				DirectPlayback.Cancel();
			else
			DirectPlayback.Play( "SitGroundPose_01" );
			bIsLock = !bIsLock;
			*/
			Ragdoll();

			Input.ClearActions();
		}

		if ( CanDuck && CanStand )
			DoDuck( Input.Down( "duck" ) );

		IsRunning = (Input.Down( "run" ));


		// TODO Делегировать данную часть кода, или нет?
		if ( Game.IsServer )
		{
			foreach ( var ef in BodyEffects )
			{
				ef.Call();
			}
		}
		if ( Input.Pressed( "run" ) )
		{


			//Log.Info( GetWeapons().ToArray().Count());
			//Log.Info( Inventory.instance.Slots.Count );
			//Log.Info( GIS.Items.Count );
			//Log.Info( GIS.Containers.Count );
			if ( Game.IsServer){
				foreach ( var items in GIS.Items.Values )
				{
					//Log.Info( items.GetData( "Ammo" ) );
				}
			}
	
			

			//if ( Game.IsClient )
			//	InventoryUpdate( 0 );
			
		}

		ActiveWeapon?.Simulate( cl );

		// TODO Перенести в соотвествующий раздел
		var tStamina = Stamina + RegenStamina * Time.Delta;
		var dsStamina = Velocity.Length / MaxSpeed > 0.5 ? Velocity.Length / MaxSpeed : 0;
		if ( dsStamina > 0f )
			Stamina -= Time.Delta * dsStamina * 15f;
		else
			Stamina = tStamina > MaxStamina ? MaxStamina : tStamina;
	}

	public bool IsThirdPerson { get; set; } = false;

	Vector3 Shakes;
	Angles HeadAngle;
	public override void FrameSimulate( IClient cl )
	{

		if ( Input.Pressed( "inventory" ) )
		{
			if ( Inventory.instance == null || !Inventory.instance.IsValid )
			{
				CreateInventory();
			}
			else
			{
				GURPSMode.LocalHUD = new CloneHUD();
				Inventory.instance.Delete();
				Inventory.instance = null;

			}
			// TODO Вернуть во имя бога оптимизации
			//InventoryPanel.SetClass( "hidden", !InventoryPanel.HasClass( "hidden" ));
		}

	

		if( !FModel.IsValid  )
		{
			FModel = new BodyModel(this);
		}


		EyePosition = (Vector3.Up * GetBody().Maxs.z) + (Rotation * CameraOffset) + Position;

		base.FrameSimulate( cl );

		if ( Camera.FirstPersonViewer == null )
		{
			FModel.Draw(false);
		}


		//TODO Сделать постоянной выгружаемой при подгрузке
		CurvesData PlayerCurves = ResourceLibrary.Get<CurvesData>( "data/css.data" );

		//TODO  Настраеваемый параметр
		var dir = DiractionCoff();
		//! Изменение угла накоола
		dir *= PlayerCurves.AngleWalk.Evaluate( (Velocity.Length / MaxSpeed) );

		//TODO При резких измененияъх значения создается баг дергания камеры.
		//! Изменение FOV-а при перемещении
		Camera.FieldOfView = Camera.FieldOfView.LerpTo( Screen.CreateVerticalFieldOfView( Game.Preferences.FieldOfView ) + (PlayerCurves.FOVWalk.Evaluate( ((Velocity * Rotation).WithZ( 0 ).Length / MaxSpeed) )), Time.Delta);

		if ( dir.Length > 0 )
			HeadAngle = HeadAngle.LerpTo( new Angles( dir.x * 2, 0, dir.y * -2.5f ), Time.Delta / 0.5f );
		else
			HeadAngle = HeadAngle.LerpTo( Angles.Zero, Time.Delta / 0.5f );


		Camera.Rotation = (ViewAngles + HeadAngle).ToRotation();

		if ( Input.Pressed( "view" ) )
		{
			IsThirdPerson = !IsThirdPerson;
			FModel.Draw(!FModel.EnableDrawing);
		}

	

		if ( IsThirdPerson )
		{
			Vector3 targetPos;
			var pos = Position + Vector3.Up * GetBody().Maxs.z;
			var rot = Camera.Rotation * Rotation.FromAxis( Vector3.Up, -16 );
			float distance = 50.0f * Scale;
			targetPos = pos + rot.Right * ((GetBody().Mins.x + 50));
			targetPos += rot.Forward * -distance;

			var tr = Trace.Ray( pos, targetPos )
				.WithAnyTags( "solid" )
				.Ignore( this )
				.Radius( 8 )
				.Run();

			Camera.FirstPersonViewer = null;
			Camera.Position = Camera.Position.LerpTo( tr.EndPosition, Time.Delta * 6 );
			//Camera.Position = tr.EndPosition;
		}
		else
		{
			Camera.FirstPersonViewer = this;

			var WalkCurve = PlayerCurves.WalkShake;
			var speedCAl = PlayerCurves.AngleWalk.Evaluate( Velocity.WithZ(0).Length / MaxSpeed );

			//var RunFactor = (((Velocity.Length / MaxSpeed) * 2));
			var RunFactor = Velocity.WithZ( 0 ).Length > WalkSpeed ? 15 : 2;



			//! Покачивание голоовы
			var pos = (Vector3.Up * GetBody().Maxs.z) + (Rotation * CameraOffset);
			pos += Position;
/*
			var RunFactor = (int)((Velocity.Length / MaxSpeed) * 12 + 1);
			HeadShake = HeadShake.LerpTo(  (new Vector3(0.5f,0.0f,1.55f) * Rotation * (float)Math.Sin(Time.Now * RunFactor  * 2))  , Time.Delta / 0.2f) ;
			if ( Velocity.WithZ( 0 ).Length > 0 )
				WalkCurve.ValueRange = new Vector2( -2, 2 );
*/
			WalkCurve.ValueRange = WalkCurve.ValueRange + new Vector2( speedCAl * 3, speedCAl * 3 );
			//WalkCurve.TimeRange = new Vector2( -1f, 0.5f );


			var wsEval = (float)Math.Sin( (Time.Now * RunFactor) );


			HeadShake = new Vector3( 0.5f, 0.0f, 1.55f ) * Rotation * WalkCurve.Evaluate( wsEval );

			var sShakes = Shakes.LerpTo( Vector3.Zero, Time.Delta / 0.5f );

			if ( !IsDucking )
			{
				pos += HeadShake;
			}
			pos += PlayerCurves.LandingShake.Evaluate( Shakes.Length / new Vector3( -5, 0, -10 ).Length ) * Shakes * Rotation;

			Shakes = sShakes;

			//HeadShake = Vector3.Zero;


			Camera.Position = pos;
		
		}

		//TODO Настраеваемый параметр + ускорение
		//Camera.Rotation = Rotation.Lerp( Camera.Rotation, ViewAngles.ToRotation(), Time.Delta * 9 );


		//Camera.ZNear = 500f;
		//Camera.ZFar = 3000.0f;
		ActiveWeapon?.FrameSimulate( cl );
	}


	public BBox GetBody()
	{
		var bodySize = PawnBody;
		if ( IsDucking )
			bodySize.Maxs = bodySize.Maxs - DuckSize;
		return bodySize * Scale;
	}
	TraceResult TracePawn( Vector3 Start, Vector3 End )
	{
		var tr = Trace.Ray( Start, End )
		.Size( GetBody() )
		.WithAnyTags( "solid", "player" )
		.Ignore( this )
		.Run();

		return tr;
	}

	[ClientRpc]
	public void BodyStatus(bool status)
	{
		FModel.Draw(status);
	}

	public void ChangePawn(Pawn newbody)
	{
		HolstetWeapon();
		BodyStatus( To.Single( Client ), false );
		this.Client.Pawn = newbody;
		UpdatePawn( newbody );
	}
	/*
		[Net]
		public AnimatedEntity PlyRagdoll { get; set; }
	*/
	
	[Net] public GCorpse Corpse { get; set; }

	public void Ragdoll(bool withBones = true)
	{
		HolstetWeapon();
		if ( IsDead )
			return;
		if (Game.IsServer) 
		{
			Corpse = new GCorpse(this);
			//Velocity = 0;
			Corpse.CopyBones();
			Position = Position.WithZ( -100 );
			LifeState = LifeState.Dead;
		}
		BodyStatus( false );
		ActiveWeapon?.Holstering();
	}


	public void UnRagdoll()
	{
		if ( IsAlive )
			return;
		if ( Game.IsServer )
		{
			//PlyRagdoll = null;
			//SetupPhysicsFromModel( PhysicsMotionType.Dynamic, false );
			LifeState = LifeState.Alive;
			Corpse.Destroy();
		}
		BodyStatus( true );
		this.EnableDrawing = true;
	}

	protected override void OnDestroy()
	{
		Event.Run( GurpsEvent.Player.Remove, this );
		/*
				if(Game.IsServer)
					Corpse?.Delete();
		*/
		if ( Game.IsServer )
		{
			Items.Destroy();
			Slots.Destroy();
		}
		else
		{

		}
		base.OnDestroy();
	}



	public static IReadOnlyList<BoneCollection.Bone> GetBoneSegment( BoneCollection.Bone bone, List<BoneCollection.Bone> isRetro = null )
	{
		if ( isRetro == null )
		{
			isRetro = new();
			isRetro.Add( bone );
		}

		foreach ( var bn in bone.Children )
		{
			isRetro.Add( bn );
			if ( bn.HasChildren )
			{
				GetBoneSegment( bn, isRetro );
			}
		}
		return isRetro;
	}

	public static void ScaleBoneSegment_Invert(ModelEntity mdl, BoneCollection.Bone bone, float scale )
	{
		var boneSegmeent = GetBoneSegment( bone );
		foreach ( var bn in mdl.Model.Bones.AllBones )
		{
			if ( !boneSegmeent.Contains( bn ) )
				mdl.SetBoneTransform( bn.Index, mdl.GetBoneTransform( bn.Index ).WithScale( scale ) );
		}
	}

	public static void ScaleBoneSegment( ModelEntity mdl, BoneCollection.Bone bone, float scale )
	{
		foreach ( var bn in GetBoneSegment( bone ) )
		{
			mdl.SetBoneTransform( bn.Index, mdl.GetBoneTransform( bn.Index ).WithScale( scale ) );
		}
	}
}
