using GURPS.Effect;
using GURPS.Inventory;
using Sandbox;
using Sandbox.Diagnostics;
using Sandbox.Internal;
using Sandbox.UI;

namespace GURPS.Core;

/// <summary>
/// ���������� �����, � ��������� �������� ��� ������ � NPC
/// </summary>
public class GCorpse : ModelEntity, IUse
{
	/// <summary>
	/// �������������� �����
	/// </summary>

	Pawn Target => Owner as Pawn;
	GTag TarTag = new();
	public GCorpse( ModelEntity ply )
	{
		Tags.Add( "solid" );
		CopyFrom( ply );
		Owner = ply;
		SetupPhysicsFromModel( PhysicsMotionType.Dynamic, false );
		Target.EnableDrawing = false;

		Transform = ply.Transform;
		Velocity = ply.Velocity;
		ply.SetParent( this );
		ply.Velocity = 0;

		/*
				TarTag.CopyFrom( ply );
				ply.Tags.Clear();
				ply.Tags.Add( "Dead" );
		*/
	}


	public void ApplyForce(Vector3 impact,  int BoneIndex = 0, Vector3 HitPose = new() )
	{
		PhysicsBody body = GetBonePhysicsBody( BoneIndex );
		if ( body == null )
			return;
		if (HitPose.Length > 0 )
		{
			body.ApplyImpulseAt( HitPose, impact  );
		}
		else
			body.ApplyImpulseAt( GetBoneTransform( BoneIndex ).Position, impact  );
	}

	public void CopyBones()
	{
		foreach ( var bn in Target.Model.Bones.AllBones )
		{
			SetBoneTransform( bn.Index, Target.GetBoneTransform( bn.Index ));
		}
	}

	[GurpsEvent.Player.Remove]
	public void DestroyPlyaer(Pawn pl)
	{
		if ( pl == Owner )
			Destroy(false);
	}
	
	public void Destroy(bool isNotFull = true)
	{
		Owner.SetParent( null );
		if( isNotFull )
		{
			Target.Velocity = PhysicsBody.Velocity;
			Target.Position = Position;
		}
		if(Game.IsServer)
			Delete();
	}

	public override void TakeDamage( DamageInfo info )
	{
		(Owner as Pawn)?.TakeDamage( info );
	}

	public GCorpse() { }

	[GameEvent.Client.Frame]
	public void CameraSimulated()
	{
		if ( Game.LocalPawn is Pawn )
		{
			var plyPawn = Game.LocalPawn as Pawn;
			if ( plyPawn.LifeState == LifeState.Dead )
			{
				var modelpl = plyPawn.Corpse;
				if ( modelpl == null )
					return;

				if ( Input.Pressed( "view" ) )
				{
					plyPawn.IsThirdPerson = !plyPawn.IsThirdPerson;
				}
				//TODO ����� �� ������� ����
				//! ����� �� ������� ����
				if ( !plyPawn.IsThirdPerson )
				{
					//! ��� �������� ���������� �� ������� ����, �������� ��� "�����������"
					//Pawn.ScaleBoneSegment( modelpl, modelpl.Model.Bones.GetBone("head") , 0 );
					//modelpl.SetBoneTransform( "head", modelpl.GetBoneTransform( "head" ).WithScale( 0 ) );
					Camera.Position = modelpl.GetBoneTransform( "head" ).Position + modelpl.GetBoneTransform( "head" ).Rotation * new Vector3( 7, -5, 0 );
					// new Angles( 0, 90, 45 )  ��� �����
					Camera.Rotation = modelpl.GetBoneTransform( "head" ).Rotation * new Angles( 0, 90, 90 ).ToRotation();
					return;
				}

				modelpl.SetBoneTransform( "head", modelpl.GetBoneTransform( "head" ).WithScale( 1 ) );
				Camera.Rotation *= Input.AnalogLook.ToRotation();
				Vector3 targetPos;
				var pos = modelpl.Position + Vector3.Up * 16;
				var rot = Camera.Rotation * Rotation.FromAxis( Vector3.Up, -5 );
				float distance = 60.0f * Scale;
				targetPos = pos;
				targetPos += rot.Forward * -distance;

				var tr = Trace.Ray( pos, targetPos )
					.WithAnyTags( "solid" )
					.Radius( 6 )
					.Run();

				tr = Trace.Ray( tr.EndPosition + tr.Direction * -10, tr.EndPosition + tr.Direction * 10 )
					.WithAnyTags( "solid" )
					.Radius( 12 )
					.Run();

				Camera.FirstPersonViewer = null;
				Camera.Position = Camera.Position.LerpTo( tr.EndPosition, Time.Delta * 6 );

			}
		}
	}

	public bool OnUse( Entity user )
	{
		if ( Game.IsServer && Target != null )
		{
			var pawn = (user as Pawn);
			if ( !Target.Items.IsConnected( pawn ) )
			{
				Target.Items.Connect( pawn );
				GIS.OpenMenu( To.Single( user ), Target.Items.ContainerId );
				return true;
			}
			if(Target.Client == user.Client )
			{
				GIS.OpenMenu( To.Single( user ), Target.Items.ContainerId );
				return true;
			}
		}
		return false;
	}

	public bool IsUsable( Entity user )
	{
		return true;
	}
}
