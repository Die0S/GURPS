using Editor;
using GURPS.Core;
using Sandbox;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using static Sandbox.DecalDefinition;

//[GameResource( "Based Data", "data", "Describes an item of clothing and implicitly which other items it can be worn with." )]
//public class WeaponS : GameResource
//{


//	public string Title { get; set; }

//	public Curve curve { get; set; }


//	[ResourceType( "vmdl" )]
//	public string Model { get; set; }

//	[HideInEditor]
//	public int Amount { get; set; }

//	[JsonIgnore]
//	public int MySecretNumber => 10;

//	// ...


//}


[GameResource( "LimnData", "limbs", "��������� ������� �����������" )]
public class LimbData : GameResource
{
	public string Name { get; set; }
	//public Color BloodColor { get; set; }

	
	[Category( "Limbs" )]
	public List<Limb> Limbs { get; set; }

}
