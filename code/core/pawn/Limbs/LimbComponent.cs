﻿using GURPS.Effect;
using Sandbox;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GURPS.Core;

[Icon( "accessible_forward" )]
public partial class LimbComponent : EntityComponent, ISingletonComponent
{
	public Color BloodColor;

	
	//public HashSet<string> BoneRoots = new();

	public LimbComponent()
	{

		if ( Game.IsServer )
		{
		}
	}

	public void Spawn()
	{
		OnSpawn();
		/*
		foreach( var item in Limbs )
		{
			BoneRoots.Add( item.mainboneName );
		}
		*/
	}
	public virtual void OnSpawn()
	{
		var hs = new Limb();
		hs.Name = "Head";
		var ds = new Limb();
		ds.Name = "Des";

		var dss = new EBleedSmall( Entity );

		hs.AddEffect( dss );
		ds.AddEffect( new EBleedLarge( Entity ) );
		ds.AddEffect( new EBleedSmall( Entity ) );

		AddLimb( hs );
		AddLimb( ds );
	}
/*
	[ClientRpc]
	public void ClientSpawn()
	{
		OnSpawn();
	}
*/
	public void AddLimb( Limb limb )
	{
		limb.Player = Entity as Pawn;
		Limbs.Add( limb );
	}

	public Limb GetLimb( string name )
	{
		foreach ( var item in Limbs )
		{
			if( item.Name == name )
				return item;
		}
		return null;
	}	
	
	public Limb GetLimb( int id )
	{
		return Limbs[id - 1];
	}

	public void AddEffect( GEffect effect, Limb plylimb )
	{
		plylimb.AddEffect( effect );
	}

	public void RemoveEffect( GEffect effect, Limb plylimb )
	{
		plylimb.RemoveEffect( effect );
	}
	[Net]
	public IList<Limb> Limbs { get; set; }


	//[GameEvent.Tick]
	[GameEvent.Tick.Server]
	public void Tick( )
	{
		foreach( var item in Limbs )
		{
			foreach(var ef in item.Effects )
			{
				ef.Call();
			}
		}
	}
}
