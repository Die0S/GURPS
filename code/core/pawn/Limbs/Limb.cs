﻿using GURPS.Effect;
using Sandbox;
using Sandbox.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GURPS.Core;

public partial class Limb : BaseNetworkable
{
	public string Name { get; set; }
	//public string TName { get; set; }
	[Net] public float MaxHealth { get; set; } = 30;
	public virtual float DefMaxHealth { get; set; } = 30;

	public Vector2 PanelOffset;

	public Pawn Player;

	/// <summary>
	/// Процент. Урон уходящий в тело, итоговый уроон уходящий в тело = Damage * DamageToBody
	/// </summary>
	[Net] public float DamageToBody { get; set; } = 0.75f;
	/// <summary>
	/// Процент. Часть урона наносимого по конечности = Damage * DamageToLimb 
	/// </summary>
	[Net] public float DamageToLimb { get; set; } = 1f;

	[Category( "Resist" )]
	public float PlasmaResist { get; set; }

	/// <summary>
	/// Главная кость нашей конечности, может потребоватться в некоторых случаях
	/// </summary>
	public string mainboneName = string.Empty;

	[Net] public float Health { get; set; }

	public GTag Tags { get; set; } = new();

	[Net] public IList<GEffect> Effects { get; set; }

	public void AddEffect( GEffect effect, int level = 1 )
	{
		if ( Game.IsServer )
		{
			if ( effect.Object == null )
				effect.Object = Player;
			if ( effect.Stuckble )
			{
				Effects.Add( effect );
				effect.Limb = this;
				effect.Active();
				effect.OnChangedLevel( 0, 1 );
			}
			else
			{
				var haseff = GetEffect( effect );
				if ( haseff != null )
					haseff.AddLevel( level );
				else
					Effects.Add( effect );
				effect.Limb = this;
			}
		}
	}

	public void RemoveEffect( GEffect effect )
	{
		if ( Game.IsServer )
		{
			effect.Deactive();
			Effects.Remove( effect );
		}
	}

	public void RemoveEffect( GEffect effect, int level )
	{
		if ( Game.IsServer )
		{
			var veff = GetEffect( effect );
			if ( veff == null ) return;
			veff.AddLevel( level * -1 );
			if ( veff.Level < 1 )
				RemoveEffect( veff );
		}
	}

	public GEffect GetEffect( GEffect effect )
	{
		if ( Game.IsServer ) 
		{
			foreach ( var ef in Effects )
			{
				if ( ef.Name == effect.Name )
					return ef;
			}
		
		}
		return null;
	}


	public Limb()
	{
		MaxHealth = DefMaxHealth;
		Health = MaxHealth;
	}	
	
	~Limb()
	{
	}



	/// <summary>
	/// Вызывается после получения урона
	/// </summary>
	/// <param name="damage"></param>
	public virtual void OnTakenDamage( float damage )
	{
	}

	/// <summary>
	/// Поломка конечности
	/// </summary>
	public void Brake()
	{
		OnBrake();
	}

	/// <summary>
	/// Нанесение урона
	/// withFactor Учитывать ли скейл уроона самой конечности
	/// </summary>
	/// <param name="damage"></param>
	/// <param name="withFactor">Учитывать ли скейл уроона самой конечности</param>
	public void TakeDamage( float damage, bool withFactor = false )
	{
		if ( IsAlive )
		{
			damage = withFactor ? damage * this.DamageToLimb : damage;
			Health -= damage;
			if ( Health <= 0 )
			{
				Brake();
				Health = 0;
			}
			OnTakenDamage( damage );
		}
	}

	public bool IsAlive => Health > 0;

	public void TakeDamage( DamageInfo info )
	{
		OnTakenDamage( info.Damage );
	}

	/// <summary>
	/// Смерть конечности. Вызывается при достижении здоровья конечности <= 0.
	/// </summary>
	public virtual void OnBrake()
	{
	}
	/*	Для подообных запросов особо актуален - [GameEvent.Tick.Server]
		public virtual void Simulate( IClient cl )
		{
		}
	*/
	public static Limb GetLimb(string bodypart)
	{
		return null;
	}

	public static Limb GetLimbByHitBox(Pawn ply , Hitbox tag )
	{
		foreach ( var limb in ply.Limbs )
		{
			if ( tag.HasAllTags( limb.Tags.hlist.ToArray() ) )
			{
				return limb;
			}
		}
		return null;
	}

}


public class GTag 
{
	public HashSet<string> hlist = new();

	public void CopyFrom(Entity target )
	{
		foreach ( var item in target.Tags.List )
		{
			hlist.Add( item );
		}
	}

	public void CopyTo(Entity target )
	{
		target.Tags.Add( hlist.ToArray() );
	}

	public bool Has( string tag )
	{
		return hlist.Contains( tag );
	}

	public bool HasAny( HashSet<string> tagList )
	{
		return hlist.Any( new Func<string, bool>( tagList.Contains ) );
	}

	public void Add( string tag )
	{
		if ( string.IsNullOrWhiteSpace( tag ) || Has( tag ) )
		{
			return;
		}

		if ( tag.Length > 32 )
		{
			return;
		}

		tag = tag.ToLowerInvariant();

		hlist.Add( tag );
	}

	public void Remove( string tag )
	{
		if ( !Has( tag ) )
		{
			return;
		}
		hlist.Remove( tag.ToLowerInvariant() );
	}

	public void Clear()
	{
		string[] array = hlist.ToArray();
		foreach ( string tag in array )
		{
			Remove( tag );
		}
	}

	public void Add( params string[] tags )
	{
		if ( tags != null && tags.Length != 0 )
		{
			foreach ( string tag in tags )
			{
				Add( tag );
			}
		}
	}

}
