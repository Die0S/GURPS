﻿using Editor;
using Sandbox;
using System.Collections.Generic;
using System;
using GURPS.Effect;
using System.Linq;

namespace GURPS.Core;

//TODO ПЕРЕПИСАТЬ API
//TODO ПЕРЕПИСАТЬ API
//TODO ПЕРЕПИСАТЬ API
partial class Pawn
{
	[Net]
	public IList<GEffect> BodyEffects { get; set; }

	public void AddEffect( GEffect effect, int level = 1 )
	{
		if ( Game.IsServer )
		{
			var haseff = GetBodyEffect( effect );
			if ( haseff != null )
			{
				haseff.AddLevel( level );
			}
			else
			{
				if ( effect.Object == null )
				{
					effect.Object = this;
					effect.Active();
					effect.OnChangedLevel( 0, 1 );
				}
				BodyEffects.Add( effect );
			}
		}
	}

	public void AddEffect( GEffect effect, Limb plylimb )
	{
		if( plylimb != null)
			plylimb.AddEffect( effect );
		else
			AddEffect( effect );
	}



	/// <summary>
	/// Уберает или уменьшаяет эффект, в зависимости от isDelete
	/// </summary>
	/// <param name="effect"></param>
	/// <param name="level"></param>
	public void RemoveEffect( GEffect effect, int level = 99 )
	{
		if ( Game.IsServer )
		{
			var veff = GetBodyEffect( effect );
			if ( veff == null ) return;
			veff.AddLevel( level * -1 );
			if ( veff.Level < 1 )
			{
				veff.Deactive();
				BodyEffects.Remove( veff );
			}
				
		}
	}


	public void RemoveEffect( int id )
	{
		if ( Game.IsServer )
		{
			foreach ( var ef in BodyEffects )
			{
				if ( ef.ID == id )
				{
					ef.Deactive();
					BodyEffects.Remove( ef );
					return;
				}
			}
			if ( Components.Get<LimbComponent>() == null )
				return;

			foreach ( var dd in Components.Get<LimbComponent>().Limbs )
			{
				foreach ( var dds in dd.Effects )
				{
					if ( dds.ID == id )
					{
						dds.Deactive();
						dd.Effects.Remove( dds );
						return;
					}
				}
			}
		}
	}

	public IList<GEffect> GetLimbList( int id )
	{
		if ( Game.IsServer )
		{
			foreach ( var ef in BodyEffects )
			{
				if ( ef.ID == id )
					return BodyEffects;
			}
			if ( Components.Get<LimbComponent>() == null )
				return null;
			foreach ( var dd in Components.Get<LimbComponent>().Limbs )
			{
				foreach ( var dds in dd.Effects )
				{
					if ( dds.ID == id )
						return dd.Effects;
				}
			}
		}
		return null;
	}



	public void RemoveEffect( GEffect effect, bool all )
	{
		if ( Game.IsServer )
		{
			if ( all )
			{
				foreach ( var ef in BodyEffects )
				{
					if( ef.Name == effect.Name )
					{
						ef.Deactive();
						BodyEffects.Remove( ef );
						return;
					}
				}
				if ( Components.Get<LimbComponent>() == null ) return;
				
				foreach ( var lb in Limbs )
				{
					foreach ( var ef in lb.Effects )
					{
						if ( ef.Name == effect.Name )
						{
							ef.Deactive();
							lb.Effects.Remove( ef );
							return;
						}
					}
				}
			}
			else
			{
				effect.Deactive();
				BodyEffects.Remove( effect );
			}
		}
	}

	public void RemoveEffect( GEffect effect, Limb plylimb )
	{
		if( plylimb != null)
			plylimb.RemoveEffect( effect );
		else
			RemoveEffect( effect );
	}

	public GEffect GetBodyEffect( GEffect effect )
	{
		foreach ( var ef in BodyEffects )
		{
			if ( ef.Name == effect.Name )
				return ef;
		}
		return null;
	}
	public GEffect GetBodyEffect( string effect )
	{
		foreach ( var ef in BodyEffects )
		{
			if ( ef.Name == effect )
				return ef;
		}
		return null;
	}

	public GEffect GetBodyEffect( int id )
	{
		foreach ( var ef in BodyEffects )
		{
			if ( ef.ID == id )
				return ef;
		}
		return null;
	}

	public IEnumerable<GEffect> GetAllEffects()
	{

		foreach ( var ef in BodyEffects )
		{
			yield return ef;
		}
		if ( Components.Get<LimbComponent>() == null )
			yield break;
		foreach ( var dd in Components.Get<LimbComponent>().Limbs )
		{
			foreach ( var dds in dd.Effects )
			{
				yield return dds;
			}
		}

	}

	public bool HasBodyEffect( string effect )
	{
		if ( Game.IsServer )
			//var allEffect = BodyEffects + Limbs.E;
			foreach ( var ef in BodyEffects )
			{
				if ( ef.Name == effect )
					return true;
			}
		return false;
	}

	public Limb GetRandomLimb()
	{
		var rand = new Random();
		if ( Components.Get<LimbComponent>() == null )
			return null;
		return Limbs[rand.Int(0,Limbs.Count-1)];
	}

}


