﻿using Editor;
using Sandbox;
using Sandbox.UI;
using System.ComponentModel;
using static Sandbox.CitizenAnimationHelper;
using static Sandbox.VisibilityBoxEntity;

namespace GURPS.Core;

[Category( "Models" )]
[Title( "BodyModel" ), Icon( "pan_tool" )]
public class BodyModel : AnimatedEntity
{
	Pawn Player => Parent as Pawn;

	public AnimatedEntity Shadow;
	
	public BodyModel(Pawn owner, Vector3 offset = new Vector3( ) ) : base( owner.Model.Name ) {
		if(offset.Length < 1)
		{
			offset = new Vector3( 12, 0, 0 );
		}
		
		Transmit = TransmitType.Never;

		SetParent( owner );
		Transform = owner.Transform;
		Position -= Rotation * offset;
		EnableShadowCasting = false;
		EnableDrawing = false;

		Shadow = new AnimatedEntity();
		Shadow.Name = Player.Name + "_Shadow";
		Shadow.SetModel( Model.Name );
		Shadow.SetParent( this );
		Shadow.Transform = Transform;

		Shadow.EnableShadowCasting = true;
		Shadow.EnableShadowOnly = true;
		Shadow.EnableShadowInFirstPerson = true;


		SetBone( "head", GetBoneTransform( "head" ).WithScale( 0 ) );
		SetBone( "arm_upper_L", GetBoneTransform( "arm_upper_L" ).WithScale( 0 ) );
		SetBone( "arm_upper_R", GetBoneTransform( "arm_upper_R" ).WithScale( 0 ) );
	}

	public void Draw( bool state = false)
	{
		Shadow.EnableDrawing = state;
		EnableDrawing = state;
	}
	public void Animate()
	{
		if ( Player != null )
		{
			var helperF = new CitizenAnimationHelper( this );
			helperF.WithVelocity( Player.Velocity );
			helperF.HoldType = Player.HoldType;
			//helperF.WithLookAt( Position + Camera.Rotation.Forward * 100 );
			helperF.IsGrounded = (Player.isGrounded);
			helperF.DuckLevel = (Player.PawnBody.Maxs.z * Scale / Player.GetBody().Maxs.z) - 1;

			var helperFs = new CitizenAnimationHelper( Shadow );
			helperFs.WithVelocity( Player.Velocity );
			helperFs.HoldType = Player.HoldType;
			helperFs.WithLookAt( Position + Camera.Rotation.Forward * 100 );
			helperFs.IsGrounded = (Player.isGrounded);
			helperFs.DuckLevel = (Player.PawnBody.Maxs.z * Scale / Player.GetBody().Maxs.z) - 1;

		}
	}
}
