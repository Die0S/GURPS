using Sandbox;
using System.Collections.Generic;
using System.Linq;

namespace GURPS.Core;

/// <summary>
/// ���������� �������������� � �������, ��������� � �.�.
/// </summary>
partial class Pawn
{

	//public IEnumerable<GWeapon> Weapons => Children.OfType<GWeapon>();
	[Net] public IList<GWeapon> Weapons { get; set; }

	public void GiveWeapon( GWeapon weapon, bool isActive = false )
	{
		Weapons.Add( weapon );
		weapon.PickUp( this, isActive );
	}

	public IEnumerable<GWeapon> GetWeapons( GWeaponType type)
	{
		foreach ( var wep in Weapons )
		{
			if ( wep.Type == type )
				yield return wep;
		}
	}

	public void RemoveWeapon( GWeapon weapon )
	{
		if ( weapon == null )
			return;
		Weapons.Remove( weapon );
		weapon.Delete();
	}

	public GWeapon WeaponByName( string weapon )
	{
		foreach(var wep in Weapons )
		{
			if(wep.Name == weapon )
				return wep;
		}
		return null;
	}

	public GWeapon WeaponByClass( string weaponClass )
	{
		foreach ( var wep in Weapons )
		{
			if ( wep.GetType().Name == weaponClass )
				return wep;
		}
		return null;
	}

	public void RemoveWeapon( string weaponClass )
	{
		RemoveWeapon( WeaponByClass( weaponClass ) );
	}

	public void RemoveWeaponByName( string weapon )
	{
		RemoveWeapon( WeaponByName( weapon ) );
	}

	public void HolstetWeapon()
	{
		ActiveWeapon?.Holstering();
		ActiveWeapon = null;
	}

	public GWeapon GetNextWeapon(GWeaponType t)
	{
		var types = GetWeapons(t).ToList();
		Log.Info( Weapons.Count() );
		if ( ActiveWeapon == null || ActiveWeapon.Type != t )
			return types.FirstOrDefault();
		var index = types.IndexOf( ActiveWeapon );
		if ( index == types.Count-1 )
			return types.FirstOrDefault();
		else
			return types[index + 1];
	}

	public void SetActiveWeapon( GWeapon weapon )
	{
		if ( ActiveWeapon == weapon )
			return;
		ActiveWeapon?.Holstering();
		ActiveWeapon = weapon;
		ActiveWeapon?.Equip();
	}

	public virtual void OnSwitchWeapon()
	{
	}

	/// <summary>
	/// ������ ������� �� �������� ������������ ��������� ��� ������
	/// </summary>
	public float FacSpeed { get; set; } = 1f;
	/// <summary>
	/// ������ �����
	/// </summary>
	public float FacDamage { get; set; } = 1f;
	/// <summary>
	/// ��������� ��������
	/// </summary>
	public float FacSpread { get; set; } = 1f;
	/// <summary>
	/// ��������� ������� ������
	/// </summary>
	public float FacKickBack { get; set; } = 1f;
	/// <summary>
	/// �������� ��������������� ������
	/// </summary>
	 public float FacEquipSpeed { get; set; } = 1f;
	/// <summary>
	/// �������� �����������
	/// </summary>
	public float FacReloadSpeed { get; set; } = 1f;
	/// <summary>
	/// �������� ������������
	/// </summary>
	public float FacZoomSpeed { get; set; } = 1f;



	//[Net] public ProFactor FactorsWeapon { get; set; } = new();

	

}
