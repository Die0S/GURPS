﻿using Editor;
using Sandbox;
using System.Collections.Generic;
using System;
using static Sandbox.CitizenAnimationHelper;
using System.Linq;

namespace GURPS.Core;

public enum PlayerState : byte
{
	IDLE,
	WALK,
	JUMP,
	SITTING,
	CLIMB,
	FLY,
}

partial class Pawn
{
	float FootStep = 20f;
	bool bChangeDuckSize = false;
	[Net] bool IsDucking { get; set; } = false;
	bool CanStand { get; set; } = true;
	//HashSet<PlayerState> ControllerEvents;
	//PlayerState ControllerEvents;
	List<PlayerState> ControllerEvents = new List<PlayerState>();
	/// *Глобальные переменные
	/// Максимальный допустимый наклон поверхности
	public int MaxGroundAngle => 45;

	public bool isGrounded => GroundEntity.IsValid();
	bool IsRunning { get; set; } = false;
	//public bool IsRunning => Velocity.Length > WalkSpeed;
	/// <summary>
	/// Направление гравитации игрока
	/// 
	/// @todo: стоит уточнить на счет явногго присутвия в рамках игры
	/// </summary>
	[Net]
	public Vector3 localGravityVector { get; set; }

	//Entity ContactEntity;

	/// <summary>
	/// Симмуляция перемещения описывающая, базовую модель игрока
	/// </summary>
	private void WalkSimulation()
	{
		var movement = InputDirection.Normal;
		var groundEntity = GetGroundEntity();

		//Log.Info( groundEntity );
		if ( groundEntity.IsValid() )
		{
			Velocity = Accelerate( GetSpeed(), false );
			if ( !isGrounded )
			{
				OnGrounded();
				Velocity = Velocity.WithZ( 0 );
				//GroundEntity = null;
			}
			// TODO Протестировать
			Velocity += groundEntity.Velocity;
		}
		else
		{
			Velocity += ViewAngles.WithPitch( 0 ).ToRotation() * movement * (Input.Down( "run" ) ? 2 : 1);
			GravityCalculate();
		}
		
		//StackFix();
	
		if ( Input.Pressed( "jump" ) )
		{
			Jump();
		}


		var MoveH = new MoveHelper( Position, Velocity, "solid", "player" );
		

		CanStand = true;
		if ( IsDucking )
		{
			var tr = Trace.Ray( Position, Position + Vector3.Up )
			.Size( GetBody().Mins, PawnBody.Maxs )
			.WithAnyTags( "solid" )
			.Ignore( this )
			.Run();

			CanStand = !tr.Hit;
		}
		//MoveH.GroundBounce = 2f;
		//MoveH.WallBounce = 2;
		MoveH.Trace = MoveH.Trace.Size( GetBody() ).Ignore( this );
		MoveH.MaxStandableAngle = MaxGroundAngle;	

		if ( MoveH.TryMoveWithStep( Time.Delta, FootStep ) > -1 )
		{
			if ( isGrounded )
			{
				MoveH.Position = GoToGround( MoveH.Position );
			}

			Position = MoveH.Position;

			//TODO Фикизика колиизии с предметами
			/*	
 			if( Game.IsServer )
			{
				var AllEntitys = Trace.Box( GetBody(), Position, Position + Velocity * Time.Delta ).DynamicOnly().RunAll();
				foreach(var entity in AllEntitys )
				{
					if( entity.Entity.IsValid() )
					{
						entity.Entity.Velocity = Rotation.Forward * Velocity.Length;
					}
				}
			
			}
		*/
			//! Обнуление скорости при столкноовении, в будующем моожем добавить дополнительную трассировку, но только через опимизацию


			if ( MoveH.Velocity.Length < MaxSpeed * Time.Delta && InputDirection.Length > 0 )
			{
				Velocity = (Vector3.Zero * Rotation).WithZ( Velocity.z );
			}
			else
				Velocity = MoveH.Velocity;
		}

		StackFix();

		GroundEntity = groundEntity;
	}

	void DoDuck( bool isDuck )
	{
		IsDucking = isDuck;
	}

	public float GetSpeed()
	{
		float stateSpeed = IsRunning ? MaxSpeed : WalkSpeed;
		stateSpeed = IsDucking ? DuckSpeed : stateSpeed;
		return stateSpeed;
	}

	private void GravityCalculate()
	{
		Velocity += Game.PhysicsWorld.Gravity * Time.Delta / 1.2f;
	}

	float GroundDist;
	float SurfaceFr;
	public Entity GetGroundEntity()
	{
		if ( Velocity.z > 100f )
			return null;

		var tr = TracePawn( Position, Position + Vector3.Down);
		if ( !tr.Hit )
			return null;

		//! Костыль в связи с тем, что возвращаемая tr.Normal почему то отличается от сервероной
		if ( tr.Entity is Pawn )
			return tr.Entity;
		if ( tr.Normal.Angle( Vector3.Up ) > MaxGroundAngle )
			return null;
		SurfaceFr = tr.Surface.Friction;
		return tr.Entity;
	}

	//TODO Проверить на рабооту вне сети [Net]
	TimeUntil timeUnJump { get; set; }
	public void Jump()
	{
		if ( timeUnJump > 0 || (!isGrounded && !FlyJump) )
			return;

		Velocity += Rotation.Up * JumpPower;
		ControllerEvents.Add( PlayerState.JUMP );

		timeUnJump = JumpTime;

	}	
	/// <summary>
	/// Приземление персонажа, для восприезведение звуков урона и дргой фигни
	/// </summary>
	public virtual void OnGrounded()
	{
		Shakes += new Vector3(-5, 0, -10);
	}

	public bool HasEvent( PlayerState eventName )
	{
		return ControllerEvents.Contains( eventName );
	}

	float stepTime = 0.05f;
	//? Возможно проблема в нарушении сетевого кода и расинхрона
	TimeUntil timeSinceLastFootstep { get; set; }
	public override void OnAnimEventFootstep( Vector3 pos, int foot, float volume )
	{
		if ( LifeState != LifeState.Alive )
			return;

		//if ( !Game.IsServer )
		if ( !Game.IsClient )
			return;

		if ( timeSinceLastFootstep > 0 )
			return;

		volume *= FootstepVolume();

		timeSinceLastFootstep = stepTime;

		//DebugOverlay.Box( 1, pos, -1, 1, Color.Red );
		//DebugOverlay.Text( pos, $"{volume}", Color.White, 5 );

		var tr = Trace.Ray( pos, pos + Vector3.Down * FootStep )
			.Ignore( this )
			.Run();

		if ( !tr.Hit ) return;

		tr.Surface.DoFootstep( this, tr, foot, volume );
	}

	public virtual float FootstepVolume()
	{
		return Velocity.WithZ( 0 ).Length.LerpInverse( 0.0f, MaxSpeed ) * 1.5f;
	}

	protected Vector3 DiractionCoff()
	{

		var inputVector = InputDirection.Normal;
		float xMod = inputVector.x > 0 ? 1f : 0.5f;
		float yMod = 0.75f;

		//? Оптимальной варианта
		if ( !IsDucking )
		{
			inputVector = inputVector.WithX( inputVector.x * xMod ).WithY( inputVector.y * yMod );
		}
		/*
				var surfaceNormal = Trace.Ray( Position, Position + Rotation.Down * 10f ).Ignore( this ).Run().Normal;
				Rotation = Rotation.Angles().WithRoll(  ( surfaceNormal * Rotation).z * 360).ToRotation();
		*/
		return inputVector;
	}

	protected Vector3 Accelerate( float wishVelocity, bool withFriction = true )
	{
		// TODO АкслТайм будет аддапртироватаься под режим хоотьюы
		float AccelTime = IsRunning ? RunAccelerate : WalkAccelerate;


		var dRunAccelerate = Velocity.Length > wishVelocity / 2 ? 1 : StartSpeedFactor;
		//var dRunAccelerate = Velocity.Length > WalkSpeed ? 1 : StartSpeedFactor;
		AccelTime = Velocity.Length > MaxSpeed ? 0.25f : AccelTime;
		//AccelTime /= dRunAccelerate;

		//! Если убрать Нормализацию (InputDirection), то боковое ускорение вернеться в "норму"
		var newDiraction = InputDirection.Normal * ViewAngles.WithPitch( 0 ).ToRotation();
		var oldVelocity = Velocity;
		var newVelocity = (DiractionCoff() * ViewAngles.WithPitch( 0 ).ToRotation() + ((newDiraction - Velocity.WithZ( 0 ).Normal) * UTrunSpeed)) * dRunAccelerate;

		var FrictionCount = withFriction ? SurfaceFr : 1f;

		// Естественное торможение
		//if ( newDiraction.Length == 0 && FrictionCount > 0  )
		if ( newDiraction.Length == 0 && FrictionCount > 0 || Velocity.Length > wishVelocity )
		{
			if ( oldVelocity.Length < 0.5f )
				oldVelocity = Vector3.Zero;
			else
				oldVelocity = oldVelocity.LerpTo( Vector3.Zero, Time.Delta / SpeedDeceleration );
		}
		else
			oldVelocity = oldVelocity.LerpTo( wishVelocity * newVelocity * FrictionCount, Time.Delta / AccelTime );
		//oldVelocity = Velocity + (wishVelocity * newVelocity * FrictionCount * Time.Delta / AccelTime);
		return oldVelocity;	}

	void AddEvent( PlayerState eventName )
	{
		if ( HasEvent( eventName ) )
			return;
		ControllerEvents.Add( eventName );
	}
	/// <summary>
	/// /Базовое воспроизведение анимаций с постоянным обновлением логики
	/// @todo: Переписать в пользу единой переменной игрока и фрагментным (в рамках всех функций) изменений его состояния.
	/// </summary>
	void AnimatePlayer()
	{
		//Log.Info( Time.Delta );
		var MainAnimation = new CitizenAnimationHelper( this );
		MainAnimation.WithWishVelocity( Velocity );
		MainAnimation.WithVelocity( Velocity );
		MainAnimation.WithLookAt( Position + ViewAngles.Forward * 1000 );
		MainAnimation.HoldType = ActiveWeapon == null ? CitizenAnimationHelper.HoldTypes.None : MainAnimation.HoldType;
		//MainAnimation.DuckLevel = IsDucking ? 1 : 0;
		MainAnimation.DuckLevel = (PawnBody.Maxs.z * Scale / GetBody().Maxs.z) - 1;
		//SetAnimParameter( "b_crouch", ((PawnBody.Maxs.z * Scale / GetBody().Maxs.z) - 1) > 0);

		//MainAnimation.IsGrounded = (GroundEntity.IsValid() || Math.Abs(Velocity.z) < 150);
		MainAnimation.IsGrounded = (isGrounded);
		//MainAnimation.IsGrounded = GroundEntity.IsValid() || Math.Abs( Velocity.z) < 50f;
		MainAnimation.MoveStyle = CitizenAnimationHelper.MoveStyles.Walk;
		if ( IsRunning )
			MainAnimation.MoveStyle = CitizenAnimationHelper.MoveStyles.Run;
		if ( HasEvent( PlayerState.JUMP ) )
			MainAnimation.TriggerJump();
		if ( Game.IsClient )
		{
			FModel.Animate();
		}
		ControllerEvents.Clear();
	}

	internal int StuckTries = 0;
	public virtual bool StackFix()
	{
		var result = TracePawn( Position, Position );

		// Не застрял
		if ( !result.StartedSolid )
		{
			StuckTries = 0;
			return false;
		}

		if ( Game.IsClient )
			return true;

		int AttemptsPerTick = 20;


		for ( int i = 0; i < AttemptsPerTick; i++ )
		{
			var pos = Position + Vector3.Random.Normal * (((float)StuckTries) / 2.0f);

			// First try the up direction for moving platforms
			if ( i == 0 )
			{
				pos = Position + Vector3.Up * 5;
			}

			result = TracePawn( pos, pos );

			if ( !result.StartedSolid )
			{
				Position = pos;
				return false;
			}
		}

		StuckTries++;

		return true;
	}

	bool bIsSiting => ControllerEvents.Contains( PlayerState.SITTING );
	void SittingSimulate()
	{
	}
	bool bIsClimb => ControllerEvents.Contains( PlayerState.CLIMB );
	void ClimbingSimulate()
	{
	}

	Vector3 GoToGround( Vector3 position )
	{
		var start = position;
		var end = position + Vector3.Down * FootStep;

		var tr = TracePawn( position, start );
		start = tr.EndPosition;

		tr = TracePawn( start, end );

		

		GroundDist = tr.EndPosition.Distance( Position );
		
		if ( tr.Fraction <= 0 ) return position;
		if ( tr.Fraction >= 1 ) return position;
		if ( tr.StartedSolid ) return position;
		if ( Vector3.GetAngle( Vector3.Up, tr.Normal ) > MaxGroundAngle ) return position;


		return tr.EndPosition;
	}

}


