﻿using Editor;
using Sandbox;
using System.Collections.Generic;
using System;
using GURPS.Effect;
using System.Linq;

namespace GURPS.Core;

/// <summary>
/// Данная часть класса содержит реализацию ВОССТРЕБОВАННЫХ решений
/// Пример: манипуляция с ХП, инвентарем и т.д.
/// </summary>

partial class Pawn
{
	/// <summary>
	/// Максимальнное значение скороси, напрямую фигурирует в  процессе премещения.
	/// </summary>
	// [Net] ? Нужна ли нам вообще скоость траснлируемая на клиент?
	public float MaxSpeed { get; set; } = 550f;
	// [Net] ? Нужна ли нам вообще скоость траснлируемая на клиент?
	public float WalkSpeed { get; set; } = 50;
	public float CrouchSpeed { get; set; } = 50;
	public float JumpPower { get; set; } = 200;

	/// <summary>
	/// КД между прыжками. Время которое должно пройти между ними
	/// </summary>
	//[Net]
	public float JumpTime { get; set; } = 0.1f;
	/// <summary>
	/// Может ли игрок прыгать в воздухе
	/// </summary>
	public bool FlyJump { get; set; } = true;

	/// <summary>
	/// Время для достижения задоного ускорения, при 50 TickRate - 1/50 *  SpeedAccelerate
	/// </summary>
	//[Net]
	public float RunAccelerate { get; set; } = 1f;
	//[Net]
	public float WalkAccelerate { get; set; } = 1.5f;


	public float StartSpeedFactor { get; set; } = 2f;

	// TODO Доделать
	/// <summary>
	/// Модификатор скорости при наклоне поверхности
	/// </summary>
	public float NormalSpeedFactor { get; set; } = 0f;

	/// <summary>
	/// Может ли персонаж использовать приседание
	/// </summary>
	public bool CanDuck { get; set; } = true;
	/// <summary>
	///  Скорость перемещения присяди
	/// </summary>
	public float DuckSpeed { get; set; } = 50f;
	/// <summary>
	/// Множитель ускорения в противоположное направление движения
	/// </summary>
	public float UTrunSpeed { get; set; } = 10f;
	/// <summary>
	/// Время торможения персонажа
	/// </summary>
	public float SpeedDeceleration { get; set; } = 0.25f;

	[Net] public float Stamina { get; set; }
	[Net] public float MaxStamina { get; set; } = 100f;
	/// <summary>
	/// Регенерация стамины в секудну
	/// </summary>
	public float RegenStamina { get; set; } = 5f;
	
	/// <summary>
	/// Сопротевление импакту
	/// </summary>
	public float FarImpactResist{ get; set; } = 1f;

	[Net] public float MaxHealth { get; set; } = 100f;

	// ? Возможно не оптимально, так как постоянный расчет
	/// <summary>
	/// Высота приседания
	/// </summary>
	Vector3 DuckSize => new Vector3( 0, 0, 32 );

	/// <summary>
	/// Максимальный вес перссонажа
	/// </summary>
	[Net] public float MaxWeight { get; set; } = 6f;

	[Net] public float Weight { get; set; } = 0;

	public Limb GetLimb( string name )
	{
		return null;
	}
	public Limb GetLimb( int id )
	{
		return null;
	}


	public void SetHealth( float health )
	{
		Health = health.Clamp( 0, MaxHealth );
		if ( Health < 1 )
			Kill();
	}
	public void AddHealth( float health )
	{
		Health += health; 
		Health.Clamp( 0, MaxHealth );
		if ( Health < 1 )
			Kill();
	}



	// TODO На случай если потребуется узнать информацию о убийстве
	public virtual void OnKilled( DamageInfo dmg)
	{

		Ragdoll();

		//if ( dmg.Force != null )
		Corpse.ApplyForce( dmg.Force, dmg.BoneIndex, dmg.Position );
	}	
	
	
	public void Kill( DamageInfo info = new())
	{
		OnKilled( info );
		//! Игрок может умерить, но для дополнительной детекции мы разделим состояния для смерти уже регдола.
		LifeState = LifeState.Dead;
	}

	public override void OnKilled()
	{
		OnKilled( new DamageInfo() );
	}


	public virtual void TakeDamage( DamageInfo info, Limb limb = null  )
	{
		
		// ? Стоит ли нам оставить расчет нанесения урона для конечности вне зависимости от состояния самого тела?
		var dmgTobody = limb != null ? limb.DamageToBody : 1;
		limb?.TakeDamage( info.Damage * limb.DamageToLimb );
		info.Damage = info.Damage * dmgTobody;

		if ( Game.IsServer && Health >= 0f && LifeState == LifeState.Alive )
		{
			Health -= info.Damage;
			//todo Дообавить поддержку OnFainted на случай когда хп доходит до 0, но позже возвращается
			if ( Health <= 0f )
			{
				Health = 0f;
				Kill( info );
			}

		}
	}

	public void TakeDamage( float damage, Limb limb = null )
	{
		TakeDamage( new DamageInfo().WithDamage(damage), limb );
	}

	/// <summary>
	/// Базовое нанесение урона с учетом АВТОМАТИЧЕСКОЙ детекции конечности
	/// </summary>
	/// <param name="info"></param>
	public override sealed void TakeDamage( DamageInfo info ) 
	{
		if(Limbs != null )
		{
			TakeDamage( info, GetLimbByHitBox( info.Hitbox ) );
		}
		TakeDamage( info, null );
	}

	public Limb GetLimbByHitBox( Hitbox tag )
	{
		foreach ( var limb in Limbs )
		{
			if ( tag.HasAllTags( limb.Tags.hlist.ToArray() ) ) 
			{
				return limb;
			}
		}
		return null;
	}


	public bool IsAlive => LifeState == LifeState.Alive;
	public bool IsDead => LifeState == LifeState.Dead;

	/// <summary>
	/// Находится ли в сознании?
	/// </summary>
	public bool IsConscious => !IsDead && Corpse != null;
	/*	
		public override void GetHoldType( DamageInfo info )
		{
			base.TakeDamage( info );
		}
	*/
}


