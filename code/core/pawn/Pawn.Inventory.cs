﻿using GURPS.Inventory;
using Sandbox;
using System.Collections.Generic;
using System.Linq;
using static Sandbox.Clothing;

namespace GURPS.Core;



partial class Pawn
{
	//GInventory Items;

	public GIContainer Items;

	public ItemSlots Slots { get; set; }


	public bool HasItem(ItemData item )
	{
		return Items.ItemList.Contains( item );
	}

	public ItemSlot GetSlot( ItemData item )
	{
		foreach(var slot in Slots.Slots )
		{
			if ( slot.Item == item )

				return slot;
		}
		return null;
	}

	public bool HasItem( ulong itemId )
	{
		return Items.ItemList.Contains( GIS.Items[itemId] );
	}

	public void Equip( ItemSlot slot, ItemData item)
	{
		if ( Slots.Slots.Contains(slot) )
		{
			item.Equip(slot );
			Slots.Update();
		}
	}

	public void Unequip( ItemSlot slot )
	{
		if ( Slots.Slots.Contains(slot) )
		{
			var itemref = slot.Item.Id;
			slot.Item.Unequip(Items.ContainerId);
			Slots.Update();
		}
	}

	public void Equip(  ItemData item )
	{
		foreach(var slot in Slots.Slots )
		{
			if ( slot.Check( item ) )
			{
				Equip( slot, item);
				return;
			}
		}
	}

	public void Unequip( ItemData item )
	{
		if ( Slots.Slots.Contains( item.Slot ) )
		{
			item.Unequip( Items.ContainerId );
			Slots.Update();
		}
	}


	/// <summary>
	/// ID Контейнеров которые видит игрок
	/// </summary>
	[Net] public IList<ulong> ContainerIds { get; private set; } = new List<ulong>();


	public void AddItem( ItemData item)
	{
		if ( Game.IsClient )
			return;
		if ( !GIS.Items.ContainsKey(item.Id) )
		{
			GIS.Items[item.Id] = item;
		}

		if( item != null )
			Items.Add(item);
	}

	public void RemoveItem( ItemData item, bool DeleteItem = true )
	{
		if ( item != null )
			Items.Remove( item );
	}

	/// <summary>
	/// * НЕ использовать
	/// </summary>
	/// <param name="container"></param>
	public void Connect( GIContainer container )
	{
		if ( !ContainerIds.Contains( container.ContainerId ) )
		{
			ContainerIds.Add( container.ContainerId );
		}
	}

	/// <summary>
	/// * НЕ использовать
	/// </summary>
	/// <param name="container"></param>
	public void Disconnect( GIContainer container )
	{
		if ( ContainerIds.Contains( container.ContainerId ) )
		{
			ContainerIds.Remove( container.ContainerId );
		}
	}

	public virtual void InventoryUpdate(ulong containerId)
	{
		Inventory.instance?.Update();
	}

	[ConCmd.Server]
	public static void Disconnect_Serv(ulong contID)
	{
		GIS.Containers[contID].Disconnect( ConsoleSystem.Caller.Pawn as Pawn );
	}

	public IEnumerable<GIContainer> Containers
	{
		get
		{
			foreach ( var id in ContainerIds )
			{
				yield return GIS.Containers[id];
			}
		}
	}

	/// <summary>
	/// Загрузка из бд данных о инвентаре
	/// </summary>
	public virtual void Load()
	{
		Items = new();
		Slots = new(this);
		Items.Entity = this;
		Items.Connect( this );

		Slots.AddSlot( "Heavy" );
		Slots.AddSlot( "Average" );
		Slots.AddSlot( "Light" );
		Slots.AddSlot( "Light" );
		Slots.AddSlot( "SPECIAL" );

		AddItem( GIS.Create( "IBackPack" ) );
		AddItem( GIS.Create( "IFRAmmo" ) );
		AddItem( GIS.Create( "ITest" ) );
		AddItem( GIS.Create( "ItemDC15A" ) );
		AddItem( GIS.Create( "ItemDC15A" ) );

		Slots.Update();
	}

	public Dictionary<GItem, int> Limit = new();

	protected virtual void LoadLimits()
	{
		var PlayerLim = ResourceLibrary.GetAll<ItemsLimit>();
		ItemsLimit itemlimit = PlayerLim.FirstOrDefault();


		if ( itemlimit.Limit != null )
		{
			foreach ( var iteml in itemlimit.Limit )
			{
				Limit.Add( GIS.GetInstance(iteml.Key), iteml.Value );
			}
		}
	}
	//TODO Пока не учитывает предметы внутри предметов
	public IEnumerable<ItemData> GetAllItems()
	{
		var newlist = Items.ItemList.ToList();
		foreach ( var item in Slots.Slots )
		{
			if( item.Item != null)
				newlist.Add( item.Item );
		}

		return newlist;
	}


	//! Технические методы

	List<GIWindow> FramesList = new();
	/// <summary>
	/// Client-ая функция для создания инвентаря
	/// </summary>
	/// <param name="container"></param>
	public void CreateInventory( GIContainer container = null )
	{
		Inventory.instance?.Delete();
		Inventory.instance = new Inventory();
		if ( container != null )
		{
			var men = new GIWindow( container );
			Inventory.instance.AddChild( men );
			FramesList.Add( men );
		}
		/* Поддержка множества окон
		else
		{
			Inventory.instance?.Delete();
			Inventory.instance = new Inventory();
		}
		*/
		Inventory.instance.MainI = new GIWindow();
		Inventory.instance.AddChild( Inventory.instance.MainI );
		FramesList.Add( Inventory.instance.MainI );

		GURPSMode.LocalHUD?.Delete();
	}

	/// <summary>
	/// Вызывается для обновления данных о инвеентаре, так как они напрямую связанны с клиентом а не Pawn-ом
	/// </summary>
	public void UpdatePawn(Pawn newpawn)
	{
		if ( Game.IsServer )
			GIContainer.Update( To.Single( Client ), newpawn.Items.Write().ToArray() );
		else
			ClientClear();
	}


	public void ClientClear()
	{
		if( Game.IsClient )
		{
			foreach ( var cont in GIS.Containers )
			{
				if ( !cont.Value.IsConnected( this ) )
					GIS.Containers.Remove( cont.Key );
			}
			foreach ( var item in GIS.Items )
			{
				if ( !GIS.Containers.ContainsKey( item.Value.ContainerId ) )
					GIS.Items.Remove( item.Key );
			}
		}
	}

}
/*
/// <summary>
/// Установленые ограничения предметов для игрока
/// </summary>
public class ItemLimit
{
	public Dictionary<string, int> ItemLimitation;
}
*/
