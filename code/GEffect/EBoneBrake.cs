﻿using GURPS.Core;
using Sandbox;
using System;
using System.Collections.Generic;

namespace GURPS.Effect;
//public class EBleed { 
public class EBoneBrake : GEffect
{
	public static EBoneBrake instance = new EBoneBrake( null );
	public EBoneBrake( Entity pl ) : base( pl ) {}
	public EBoneBrake() { }
	////

	public override string Name => "#effect.bonebrake";

	public override void OnCreate()
	{
		Description = "Что то не так с вашим телом...";
		MaxLevel = 4;
		Stuckble = true;
		Icon = "https://cdn-icons-png.flaticon.com/512/2853/2853271.png";
	}

	float SpeedFactor = 1.25f;

/*
	public override void Deactive() {
		Player.MaxSpeed *= Level * SpeedFactor;
	}
*/
	public override void OnChangedLevel( int oldValue, int newValue )
	{
		//oldValue = oldValue > 0 ? oldValue : 1;
		if( oldValue > 0 )
		Player.MaxSpeed *= oldValue * SpeedFactor;
		if ( newValue > 0 )
		Player.MaxSpeed /= newValue * SpeedFactor;
	}

	public override void Call()
	{

	}

}
