﻿using GURPS.Core;
using Sandbox;
using System;
using System.Collections.Generic;

namespace GURPS.Effect;
public class EBleedSmall : GEffect
{
	public static EBleedSmall instance = new EBleedSmall( null );
	public EBleedSmall( Entity pl ) : base( pl ) {}
	public EBleedSmall() { }
	////

	public override string Name => "#effect.bleeds";

	public float BleedCount = 1f;
	public float BleedTime = 5f;
	public TimeSince lasBleed = 0;

	public override void OnCreate()
	{
		Description = "Вы чувствуете легкую потерю крови, лучше поторопится с решением этой проблемы!";
		MaxLevel = 3;
		Icon = "https://cdn-icons-png.flaticon.com/512/967/967119.png";
	}

	public override void Active() { }
	public override void Deactive() { }

	//x[GameEvent.Tick] #СПАСИБО СБОРЩИКУ МУСОРА
	/*
	Пример небольшого, но постоянно события кровопотери.
	*/

	public override void OnChangedLevel( int oldValue, int newValue )
	{
		if ( oldValue == newValue)
		{
			if( Player.IsValid() ) 
			{ 
				//! Удаляет и добавляет эффект, в случае отсутвия указанной конечности будет добавлять на тело
				Player.AddEffect( new EBleedLarge( Player ) , Limb);
				Player.RemoveEffect( this, Limb );
			}
		}
	}
	
	/// <summary>
	/// Наноосит урон игроку и конечности в случае существования таковой
	/// </summary>
	public override void Call()
	{
		if ( lasBleed > BleedTime )
		{
			Player.TakeDamage( BleedCount * Level, Limb );
			lasBleed = 0;
		}
	}
}


public partial class EBleedLarge : EBleedSmall
{
	// Создаем инстансы для удобного доступа к конфигуура
	public static new EBleedLarge instance = new EBleedLarge( null );
	public EBleedLarge( Entity pl ) : base( pl ){}
	public EBleedLarge() { }
	////

	public override string Name => "#effect.bleedl";

	public override void OnChangedLevel( int oldValue, int newValue )
	{

	}

	public override void OnCreate()
	{
		Description = "Привкус крови не дает покоя, но ее почти не осталось...";
		//MaxLevel = 5;
		BleedCount = 3f;
		BleedTime = 4f;
		Icon = "https://cdn-icons-png.flaticon.com/512/967/967119.png";
		//TODO Убрать
		Stuckble = false;
	}

	/// <summary>
	/// Наносит урон конечности или телу в зависимости от эффекта
	/// </summary>
	public override void Call()
	{
		if ( lasBleed > BleedTime )
		{
			if ( IsLimb )
			{
				Limb.TakeDamage( BleedCount * Level );
				lasBleed = 0;
			}
			else
			{
				Player.TakeDamage( BleedCount * Level );
				lasBleed = 0;
			}
		}
	}
}
//}
